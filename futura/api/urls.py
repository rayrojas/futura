from django.conf.urls.defaults import patterns, url, include
from api.views import ApirPaciente, ApirvedProfesionalCalc, ApirPacientePaquete, ApirdPacientePaquete, ApirPacPaqSer, ApirInFromPacPaq, ApirdpPacientePaquete, ApirServicio, ApirProfesional, ApirdpServicio

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
	url('^core/', include(ApirPaciente().urls)),
	url('^core/', include(ApirvedProfesionalCalc().urls)),
	url('^core/', include(ApirPacientePaquete().urls)),
	url('^core/', include(ApirdPacientePaquete().urls)),
	url('^core/', include(ApirPacPaqSer().urls)),
	url('^core/', include(ApirInFromPacPaq().urls)),
	url('^core/', include(ApirdpPacientePaquete().urls)),
	url('^core/', include(ApirServicio().urls)),
	url('^core/', include(ApirdpServicio().urls)),
	url('^core/', include(ApirProfesional().urls)),
)
