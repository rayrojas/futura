from django.utils import simplejson
from django.core.serializers import json
from tastypie.resources import ModelResource, fields
from tastypie.authorization import Authorization
from tastypie.serializers import Serializer
from sable.models import Paciente, Servicio, ProfesionalCalc, PacientePaquete, Paquete, PaqueteServicio, PacPaqSer, InFromPacPaq, Profesional


class ApirPaciente(ModelResource):
	class Meta:
		queryset = Paciente.objects.filter(activo = 'T')
		resource_name = "r/paciente"
		limit = 15
		filtering = {
			'dni' : ['exact',],
			'id' : ['exact',],
		}

# read value estado d
class ApirvedProfesionalCalc(ModelResource):
	servicio = fields.CharField(attribute = 'servicio__id')
	servicio_text = fields.CharField(attribute = 'servicio__nombre')
	servicio_precio = fields.CharField(attribute = 'servicio__precio')
	class Meta:
		queryset = ProfesionalCalc.objects.filter(estado = 'D').order_by('horainicio')
		resource_name = "rved/cupos"
		limit = 15
		filtering = {
			'servicio' : ['exact',],
			'id' : ['exact',],
			'dia' : ['exact',],
			'horainicio' : ['lte', 'gte']
		}
	def dehydrate(self, bundle):
		bundle.data['profesional_text'] = bundle.obj.profesional.get_Pseudo()
		bundle.data['dia_text'] =  bundle.obj.get_dia_text()
		bundle.data['horainicio_text'] =  bundle.obj.horainicio.strftime("%H:%M")
		bundle.data['horafin_text'] =  bundle.obj.horafin.strftime("%H:%M")
		return bundle

class ApirPacientePaquete(ModelResource):
	paciente_dni = fields.CharField(attribute = 'paciente__dni')
	paquete = fields.CharField(attribute = 'paquete__id')
	paquete_text = fields.CharField(attribute = 'paquete__nombre')
	paquete_precio = fields.CharField(attribute = 'paquete__precio')
	class Meta:
		queryset = PacientePaquete.objects.filter()
		resource_name = "r/pacientepaquete"
		limit = 15
		filtering = {
			'paciente_dni' : ['exact',],
			'paciente' : ['exact',],
			'paquete' : ['exact',],
		}
	def dehydrate(self, bundle):
		bundle.data['debe'] = bundle.obj.get_Pagos()
		return bundle

class ApirdPacientePaquete(ModelResource):
	paciente_dni = fields.CharField(attribute = 'paciente__dni')
	paquete = fields.CharField(attribute = 'paquete__id')
	paquete_text = fields.CharField(attribute = 'paquete__nombre')
	paquete_precio = fields.CharField(attribute = 'paquete__precio')
	class Meta:
		queryset = PacientePaquete.objects.filter()
		resource_name = "rd/pacientepaquete"
		limit = 15
		filtering = {
			'id' :  ['exact',],
			'paciente_dni' : ['exact',],
			'paciente' : ['exact',],
			'paquete' : ['exact',],
		}
	def dehydrate(self, bundle):
		bundle.data['debe'] = bundle.obj.get_Pagos()
		bundle.data['sesiones'] = bundle.obj.get_Sesions()
		return bundle

class ApirdpPacientePaquete(ModelResource):
	paciente_dni = fields.CharField(attribute = 'paciente__dni')
	paquete = fields.CharField(attribute = 'paquete__id')
	paquete_text = fields.CharField(attribute = 'paquete__nombre')
	paquete_precio = fields.CharField(attribute = 'paquete__precio')
	class Meta:
		queryset = PacientePaquete.objects.filter()
		resource_name = "rdp/pacientepaquete"
		limit = 15
		filtering = {
			'id' :  ['exact',],
			'paciente_dni' : ['exact',],
			'paciente' : ['exact',],
			'paquete' : ['exact',],
		}
	def dehydrate(self, bundle):
		bundle.data['pagos'] = bundle.obj.get_Pagos_json()
		bundle.data['status'] = bundle.obj.get_Pagos_status_json()
		return bundle

class ApirPacPaqSer(ModelResource):
	servicio_text = fields.CharField(attribute = 'paqser__servicio__nombre')
	class Meta:
		queryset = PacPaqSer.objects.all()
		resource_name = "r/sesion"
		limit = 15
		filtering = {
			'id' : ['exact',],
		}
	def dehydrate(self, bundle):
		bundle.data['estado_text'] = bundle.obj.get_Estado_text()
		bundle.data['terapeuta_text'] = bundle.obj.servicio.profesional.get_Pseudo()
		bundle.data['horainicio_text'] =  bundle.obj.servicio.horainicio.strftime("%H:%M")
		bundle.data['horafin_text'] =  bundle.obj.servicio.horafin.strftime("%H:%M")
		bundle.data['dia_text'] =  bundle.obj.servicio.get_dia_text()
		return bundle

class ApirInFromPacPaq(ModelResource):
	paquete = fields.CharField(attribute = 'paquete__id')
	paciente = fields.CharField(attribute = 'paciente__id')
	class Meta:
		queryset = InFromPacPaq.objects.all()
		resource_name = "r/ingresos->paquete"
		limit = 15
		filtering = {
			'id' : ['exact',],
			'paquete' : ['exact',],
		}

class ApirServicio(ModelResource):
	class Meta:
		queryset = Servicio.objects.filter(disponible = True)
		resource_name = "r/servicio"
		limit = 50
		filtering = {
			'id' : ['exact',],
		}

class ApirdpServicio(ModelResource):
	class Meta:
		queryset = Servicio.objects.filter(disponible = True)
		resource_name = "rdp/servicio"
		limit = 50
		filtering = {
			'id' : ['exact',],
		}
	def dehydrate(self, bundle):
		bundle.data['profesionales'] = bundle.obj.get_Profesionales_json()
		return bundle

class ApirProfesional(ModelResource):
	class Meta:
		queryset = Profesional.objects.filter(is_active = True)
		resource_name = "r/profesional"
		limit = 50
		filtering = {
			'id' : ['exact',],
		}
		excludes = ['password']