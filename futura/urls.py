from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
	url(r'^', include('sable.urls')),
	url(r'^usuario/', include('security.urls')),
	#url(r'^ubigeo/', include('ubigeo.urls')),
	#url(r'^reportes/', include('cruger.urls')),
	url(r'^api/', include('api.urls')),
	#url(r'^contabilidad/', include('contable.urls')),
	url(r'^admin/', include(admin.site.urls)),	
)

