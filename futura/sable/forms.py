# -*- coding: utf-8 -*-
from django import forms
from django.db import models
from sable.models import ProfesionalCalc, PacienteCalc, InFromPacPaq

class ProfesionalCalcFormInsert(forms.ModelForm):
	class Meta:
		model = ProfesionalCalc

class PacienteCalcFormInsert(forms.ModelForm):
	class Meta:
		model = PacienteCalc

class InFromPacPaqFormInsert(forms.ModelForm):
	class Meta:
		model = InFromPacPaq
		exclude = ['numeroboleta', 'numerofactura', 'nombrePagador', 'retencion']