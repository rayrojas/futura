#encoding:utf-8
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.models import User, Group
from django.contrib import messages
from django.utils import simplejson
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth import login as auth_login
from django.http import HttpResponseRedirect, HttpResponse
from django.db.models import Count

from security.forms import LoginForm
from security.views import isLogin, hasPermission
from security.models import Usuario

from sable.models import Paciente, Servicio, ProfesionalCalc, Profesional, PacientePaquete
from sable.forms import PacienteCalcFormInsert, ProfesionalCalcFormInsert, InFromPacPaqFormInsert

from sislog import logear
from datetime import date, datetime, time, timedelta
import json
import traceback
import sys

def getAplicacion():
	return {
		'site_root' : 'http://127.0.0.1:8000/',
		'root' : '/',
		'app' : {'nombre' : 'Sistema de Información Calida de Vida'}
	}

def index(request):
	usuario = request.user
	if not (isLogin(usuario)):
		return HttpResponseRedirect("/login/")
	try:
		usuario = Usuario.objects.get(id = usuario.id)
	except Exception, e:
		messages.error(request, "No se encontró un usuario con código: " + str(usuario.id))
		error = traceback.extract_stack()[-1]
		logear(str(datetime.datetime.now()) + " - " + ", ".join([type(e).__name__, str(error[0]), str(error[2]), str(error[1])]))
		return HttpResponseRedirect("/404/")
	context = {
		'aplicacion' : getAplicacion(),
		'usuario' : usuario,
	}
	return render_to_response('generals/index.html', context, context_instance = RequestContext(request))
	
def nofound(request):
	return render_to_response('nofound.html', {}, context_instance = RequestContext(request))

def login(request):
	if request.method == 'POST':
		form = LoginForm(request.POST)
		if form.is_valid():
			username = form.cleaned_data['username']
			passwd = form.cleaned_data['password']
			print username
			print passwd
			user = authenticate(username = username, password = passwd)
			if user is not None and user.groups.all():
				if user.is_active:
					auth_login(request, user)
					return HttpResponseRedirect('/')
				else:
					context = {
						'formlogin' : form,
					}
			else:
				context = {
					'formlogin' : form,
				}
		else:
			form = LoginForm()
			context = {
				'formlogin' : form,
			}
	else:
		form = LoginForm()
		context = {
			'formlogin' : form,
		}
					
	return render_to_response('generals/login.html', context, context_instance = RequestContext(request))
	
def onlogout(request):
	logout(request)
	return HttpResponseRedirect('/')

@login_required
def json_paciente_search_dni(request):
	usuario = request.user
	if (hasPermission(usuario.groups.all(), 1) or hasPermission(usuario.groups.all(), 3) or hasPermission(usuario.groups.all(), 4)):
		doReturnText = []
		doReturnVal = []
		for x in Paciente.objects.filter(dni__startswith = request.GET['query']):
			doReturnText.append(x.dni)
			doReturnVal.append(x.dni)

		doReturn = {
			'query' : request.GET['query'],
			'suggestions' : doReturnText,
			'data' : doReturnVal
		}
		return HttpResponse(simplejson.dumps(doReturn), mimetype='application/json')
	return HttpResponse(status=400)

@login_required
def json_paciente_search_nombres(request):
	usuario = request.user
	if (hasPermission(usuario.groups.all(), 1) or hasPermission(usuario.groups.all(), 3) or hasPermission(usuario.groups.all(), 4)):
		doReturnText = []
		doReturnVal = []
		for x in Paciente.objects.filter(appaterno__istartswith = request.GET['query'], activo="T"):
			if not x.dni in doReturnVal:
				doReturnText.append(x.appaterno + " " + x.apmaterno + ", " + x.nombre)
				doReturnVal.append(x.dni)
		if len(request.GET['query'].split(" "))>1:
			for x in Paciente.objects.filter(appaterno__istartswith = request.GET['query'][0:-1*(len(request.GET['query'].split(" ")[-1])+1)], apmaterno__istartswith = request.GET['query'].split(" ")[-1], activo="T"):
				if not x.dni in doReturnVal:
					doReturnText.append(x.appaterno + " " + x.apmaterno + ", " + x.nombre)
					doReturnVal.append(x.dni)
			for x in Paciente.objects.filter(nombre__istartswith = request.GET['query'][0:-1*(len(request.GET['query'].split(" ")[-1])+1)], appaterno__istartswith = request.GET['query'].split(" ")[-1], activo="T"):
				if not x.dni in doReturnVal:
					doReturnText.append(x.appaterno + " " + x.apmaterno + ", " + x.nombre)
					doReturnVal.append(x.dni)

		doReturn = {
			'query' : request.GET['query'],
			'suggestions' : doReturnText,
			'data' : doReturnVal
		}
		return HttpResponse(simplejson.dumps(doReturn), mimetype='application/json')
	return HttpResponse(status=400)	

@login_required
def json_servicio_search(request):
	usuario = request.user
	if (hasPermission(usuario.groups.all(), 1) or hasPermission(usuario.groups.all(), 3) or hasPermission(usuario.groups.all(), 4)):
		doReturnText = []
		doReturnVal = []
		for x in Servicio.objects.filter(nombre__icontains = request.GET['query'], disponible=True):
			if not x.id in doReturnVal:
				doReturnText.append(x.nombre)
				doReturnVal.append(x.id)

		doReturn = {
			'query' : request.GET['query'],
			'suggestions' : doReturnText,
			'data' : doReturnVal
		}
		return HttpResponse(simplejson.dumps(doReturn), mimetype='application/json')
	return HttpResponse(status=400)	

@login_required
def json_profesional_disponible(request):
	usuario = request.user
	if (hasPermission(usuario.groups.all(), 1) or hasPermission(usuario.groups.all(), 3) or hasPermission(usuario.groups.all(), 4)):
		doReturn = []
		for x in ProfesionalCalc.objects.filter(estado = "D").values('profesional').annotate(dcount=Count('profesional')):
			profesional = Profesional.objects.get(id = x['profesional'])
			doReturn.append({
				'profesional_text' : profesional.get_Pseudo(),
				'hoy' : profesional.get_Disponibilidad(),
				'count' : x['dcount']
			})

		return HttpResponse(simplejson.dumps({'objects':doReturn}), mimetype='application/json')
	return HttpResponse(status=400)	

@login_required
def json_cita_save(request):
	usuario = request.user
	if (hasPermission(usuario.groups.all(), 1) or hasPermission(usuario.groups.all(), 3) or hasPermission(usuario.groups.all(), 4)):
		doReturn = []
		if request.method == 'POST':
			try:
				paciente = Paciente.objects.get(dni = request.POST['paciente'])
			except Exception, e:
				error = traceback.extract_stack()[-1]
				logear(str(datetime.now()) + " - " + ", ".join([type(e).__name__, str(error[0]), str(error[2]), str(error[1])]))
				return HttpResponse(status = 401)
			#ProfesionalCalcFormInsert()
			form = PacienteCalcFormInsert({
				'profesional' : request.POST['profesionalcalc'],
				'paciente' : paciente.id,
				'precio' : request.POST['precio'],
				'precio_bk' : 0,
				'tipopago' : 'C',
				'estado' : 'D',
				'ispago' : 'F',
				'tipocita': 'C',
				'usuariox' : usuario,
				'observacion' : request.POST['observacion'],
			})
			if form.is_valid():
				print "save"
				return HttpResponse(status=200)
			else:
				print form.errors
				return HttpResponse(status=400)
	return HttpResponse(status=400)

@login_required
def json_paquete_pago_save(request):
	usuario = request.user
	if (hasPermission(usuario.groups.all(), 1) or hasPermission(usuario.groups.all(), 3) or hasPermission(usuario.groups.all(), 4)):
		doReturn = []
		if request.method == 'POST':
			try:
				paquete = PacientePaquete.objects.get(id = request.POST['paquete'])
			except Exception, e:
				error = traceback.extract_stack()[-1]
				logear(str(datetime.now()) + " - " + ", ".join([type(e).__name__, str(error[0]), str(error[2]), str(error[1])]))
				return HttpResponse(status = 401)
			form = InFromPacPaqFormInsert({
				'paquete' : paquete.id,
				'paciente' : paquete.paciente.id,
				'precio' : request.POST['monto'],
				'estado' : 'I',
				'cerrado' : 'F',
				'usuariox' : usuario.id,
				'fechaemision' : datetime.now(),
				'fechaeprox' :  datetime.now(),
			})
			if form.is_valid():
				form.save()
				return HttpResponse(status=200)
			else:
				print form.errors
				return HttpResponse(status=401)
	return HttpResponse(status=400)

@login_required
def json_profesional_disponible_calendar(request):
	usuario = request.user
	if (hasPermission(usuario.groups.all(), 1) or hasPermission(usuario.groups.all(), 3) or hasPermission(usuario.groups.all(), 4)):
		if 'profesional' in request.GET and 'start' in request.GET and 'end' in request.GET:
			try:
				profesional = Profesional.objects.get(id = request.GET['profesional'])
			except Exception, e:
				error = traceback.extract_stack()[-1]
				logear(str(datetime.now()) + " - " + ", ".join([type(e).__name__, str(error[0]), str(error[2]), str(error[1])]))
				return HttpResponse(status = 401)
			startParam = datetime.fromtimestamp(float(request.GET['start']))
			endParam = datetime.fromtimestamp(float(request.GET['end']))
			print startParam
			print endParam
			diff = endParam - startParam
			doReturn = []
			if diff.days == 1:
				for x in ProfesionalCalc.objects.filter(estado = 'D', profesional = request.GET['profesional'], dia = startParam):
					doReturn.append({
						'title' : x.profesional.get_Pseudo(),
						'start': str(x.dia) + " " + str(x.horainicio),
						'end': str(x.dia) + " " + str(x.horafin),
						'id' : x.id,
						'allDay' : False,
					})
			else:
				for x in ProfesionalCalc.objects.filter(estado = 'D', profesional = request.GET['profesional'], dia__gte = startParam, dia__lte = endParam).values('dia').annotate(dcount=Count('dia')):
					doReturn.append({
						'title' : str(x['dcount']) + " disponibles",
						'start': x['dia'].strftime("%Y-%m-%d") + " " + str('01:00'),
						'end': x['dia'].strftime("%Y-%m-%d") + " " + str('23:00'),
					})
			return HttpResponse(simplejson.dumps(doReturn), mimetype='application/json')
	return HttpResponse(status=400)

@login_required
def json_profesional_disponible_calendar_day(request):
	usuario = request.user
	if (hasPermission(usuario.groups.all(), 1) or hasPermission(usuario.groups.all(), 3) or hasPermission(usuario.groups.all(), 4)):
		if 'profesional' in request.GET and 'start' in request.GET and 'end' in request.GET and 'day' in request.GET:
			try:
				profesional = Profesional.objects.get(id = request.GET['profesional'])
			except Exception, e:
				error = traceback.extract_stack()[-1]
				logear(str(datetime.now()) + " - " + ", ".join([type(e).__name__, str(error[0]), str(error[2]), str(error[1])]))
				return HttpResponse(status = 401)
			startParam = datetime.fromtimestamp(float(request.GET['start']))
			endParam = datetime.fromtimestamp(float(request.GET['end']))
			diff = endParam - startParam
			print diff
			if diff.days == 1:
				print "uno"
				print request.GET['day']
				query = ProfesionalCalc.objects.filter(estado = 'D', profesional = request.GET['profesional'], dia__gte = startParam, dia__lte = endParam)
			else:
				print "muchos"
				query = ProfesionalCalc.objects.filter(estado = 'D', profesional = request.GET['profesional'], dia = request.GET['day'])
			doReturn = []
			for x in query:
				doReturn.append({
					'title' : "disponibles",
					'start': str(x.dia) + " " + str(x.horainicio),
					'end': str(x.dia) + " " + str(x.horafin),
					'allDay' : False,
				})
			return HttpResponse(simplejson.dumps(doReturn), mimetype='application/json')
	return HttpResponse(status=400)