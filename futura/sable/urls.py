from django.conf.urls.defaults import patterns, url
from sable import views

urlpatterns = patterns('',
	url('^$', views.index),
	url('^login/$', views.login),
	url('^bk/paciente/search/nombres.json/$', views.json_paciente_search_nombres),
	url('^bk/paciente/search/dni.json/$', views.json_paciente_search_dni),
	url('^bk/servicio/search.json$', views.json_servicio_search),
	url('^bk/profesional/disponible.json/$', views.json_profesional_disponible),
	url('^bk/paciente/cita/save.json/$', views.json_cita_save),
	url('^bk/paciente/paquete/pago/save.json/$', views.json_paquete_pago_save),
	url('^bk/profesional/disponible/calendar.json$', views.json_profesional_disponible_calendar),
	url('^bk/profesional/disponible/calendar.day.json$', views.json_profesional_disponible_calendar_day)
)
