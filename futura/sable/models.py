# -*- coding: UTF-8 -*-
from django.db import models
from django.utils.encoding import smart_str, smart_unicode
from datetime import date, datetime, time, timedelta
from django.contrib.auth.models import User, UserManager, Group

from security.models import Usuario		

class Paciente(models.Model):
	dni = models.CharField(max_length = 8, verbose_name = 'Documento Nacional de Identidad', help_text = "Si no posee Dni, llenar con 0. Ejemplo: 00000000", unique = True)
	historia = models.PositiveIntegerField(verbose_name = "Historia", help_text = "Ingrese un número Historia.", unique = True)
	nombre = models.CharField(max_length = 70, verbose_name = "Nombres", help_text = "Ingrese el Nombre.", null = False, blank = False)
	appaterno = models.CharField(max_length = 70, verbose_name = "Apellido Paterno", help_text = "Ingrese el Apellido Paterno.", null = False, blank = False)
	apmaterno = models.CharField(max_length = 70, verbose_name = "Apellido Materno", help_text = "Ingrese el Apellido Materno.", null = False, blank = False)
	nacimiento = models.DateField(verbose_name = "Fecha de Nacimiento", help_text = "Seleccione la fecha de nacimiento.", default = date.today(), null = False, blank = True)
	OPC = [["s","Soltero"], ["c", "Casado"], ["d", "Divorciado"], ["v", "Viudo"], ["d", "Menor de Edad"]]
	civil = models.CharField(max_length = 1, verbose_name = "Estado civil", help_text = "Seleccione el Estado Civil.", choices = OPC, blank = True, null = False )
	#distrito = models.ForeignKey(Ubicacion, verbose_name = "Ubicación", help_text = "Seleccione la ubicación.")
	direccion = models.CharField(max_length = 250, verbose_name = "Domicilio", help_text = "Ingrese el domicilio.", blank = True, null = False )
	referencia = models.CharField(max_length = 250, verbose_name = "Procedencia", help_text = "Ingrese la procedencia.", blank = True, null = False)
	telfijo = models.CharField(max_length = 15, verbose_name = "Teléfono fijo", help_text = "Ingrese el teléfono fijo.", blank = True, null = False)
	telmobil = models.CharField(max_length = 15, verbose_name = "Teléfono móbil", help_text = "Ingrese el teléfono móbil.", blank = True, null = False)
	rpm = models.CharField(max_length = 15, verbose_name = "Teléfono RPM/RPC", help_text = "Ingrese el número RPM/RPC.", blank = True, null = False)
	email = models.EmailField(verbose_name = "Correo Eléctronico", help_text = "Ingrese el correo eléctronico", blank = True, null = False)
	ocupacion = models.CharField(max_length = 250, verbose_name = "Ocupación", help_text = "Ingrese la ocupación.", blank = True, null = False)
	referencia_llego = models.CharField(max_length = 250, verbose_name = "Referencia", help_text = "Ingrese la referencia.", blank = True, null = False)
	diaregistro = models.DateField(auto_now = False, verbose_name="Fecha de Filiación", help_text = "Ingrese la Fecha de Filiación", null = False, blank = False, default = date.today())
	OPCH = [["T","Habilitado"], ["F", "Desabilitado"]]
	activo = models.CharField(max_length = 1, verbose_name = "Estado", help_text = "Seleccione el estado.", choices = OPCH, default = "T", blank = False, null = False)
	usuariox = models.ForeignKey(Usuario, verbose_name = "Usuario")
	def __unicode__(self):
		return u'%s' % (self.appaterno + " " + self.apmaterno  + " " + self.nombre)

	class Meta:
		app_label = 'clinica'

	def get_Civil(self):
		for listE in self.OPC:
			if self.civil in listE:
				return str(listE[1])
		return ""
	def get_Activo(self):
		for listE in self.OPCH:
			if self.activo in listE:
				return str(listE[1])
		return ""		
	def xget_Edad(self):
		today = date.today()
		try: # raised when birth date is February 29 and the current year is not a leap year
			birthday = self.nacimiento.replace(year=today.year)
		except ValueError:
			birthday = self.nacimiento.replace(year=today.year, day=self.nacimiento.day-1)
		if birthday > today:
			if (today.year - self.nacimiento.year - 1) <= 0:
				return "Edad no válida"
			else:
				return str(today.year - self.nacimiento.year - 1) + " años"
		else:
			if (today.year - self.nacimiento.year) <= 0:
				return str(today.month - self.nacimiento.month) + " meses"
			else:
				return str(today.year - self.nacimiento.year) + " años"
	def get_Historia_Clinica(self):
		doReturn = FichaMedica.objects.filter(paciente = self.id).order_by('-horaregistro')[:1]
		print doReturn
		if doReturn:
			return doReturn[0]
		else:
			[]


class Servicio(models.Model):
	nombre = models.CharField(max_length = 100, verbose_name = "Nombre del Servicio", help_text = "Ingrese el Nombre del Servicio", blank = False, null = False )
	disponible = models.BooleanField(default = True, verbose_name = "Disponible", help_text = "Indique la disponibilidad.", null = False, blank = False)
	descripcion = models.CharField(max_length = 250, verbose_name = "Descripción del servicio", help_text = "Ingrese la descripción del servicio. (max 250 caracteres)", blank = False, null = False)
	tiempo = models.IntegerField(verbose_name = "Duración del servicio", help_text = "Ingrese la duración del servicio. (minutos)", blank = False, null = False)
	precio = models.DecimalField(verbose_name = "Precio del servicio", max_digits = 7, decimal_places = 2, help_text = "Ingrese el precio del servicio.", blank = False, null = False)
	documentos = models.CharField(max_length = 250, verbose_name = "Documentos", help_text = "Seleccione los documentos a expedir.", blank = False, null = False)
	def __unicode__(self):
		return u'%s' % (self.nombre)

	class Meta:
		app_label = 'clinica'

	def get_is_informe(self):
		if "ii" in self.documentos.split(","):
			return True
		else:
			return True

	def get_Profesionales_json(self):
		doReturn = []
		for x in Profesional.objects.filter(especialidad = self.id):
			if ProfesionalCalc.objects.filter(profesional = x.id, estado = 'D'):
				doReturn.append({
					'id' : x.id,
					'username' : x.username,
					'hoy' : x.get_Disponibilidad(),
					'color' : '#' + str(x.color),
					'terapeuta_text' : smart_str(x.get_Pseudo())
				})
		return doReturn

class Profesional(User):
	direccion = models.CharField(max_length = 250, verbose_name = "Dirección", help_text = "Ingrese la dirección.", null = False, blank = True)
	telfijo = models.CharField(max_length = 15, verbose_name = "Teléfono fijo", help_text = "Ingrese el teléfono fijo.", null = False, blank = False)
	telmobil = models.CharField(max_length = 15, verbose_name = "Teléfono móbil", help_text = "Ingrese el teléfono móvil.", null = False, blank = False)
	especialidad =	models.ManyToManyField(Servicio, verbose_name = "Especialidad")
	color = models.CharField(max_length = 15, verbose_name = "Color", help_text = "Ingrese el color representativo.", null = False, blank = False)
	objects = UserManager()
	
	def __unicode__(self):
		return  u'%s' % (self.last_name + " " + self.first_name)
		
	def is_doctor(self):
		for uu in self.groups.all():
			if uu.id == 6:
				return True		
		return False

	def get_Servicios(self, tipo = "text"):
		if tipo == "text":
			doReturn = ""
			for x in self.especialidad.all():
				doReturn += " " + x.nombre + ","
			return doReturn

	def get_Pseudo(self):
		if self.first_name and self.last_name:
			return (self.first_name[0] + self.last_name.split(" ")[0])
		else:
			return self.first_name + " " + self.last_name

	def get_Disponibilidad(self, fecha = datetime.now()):
		return ProfesionalCalc.objects.filter(profesional = self.id, estado = "D", dia = fecha).count()

	class Meta:
		app_label = 'clinica'

class ProfesionalCalc(models.Model):
	dia = models.DateField(verbose_name="Ingrese un día de la semana", help_text="Ingrese un dia de la semana.", default = date.today())
	horainicio = models.TimeField(verbose_name = "Hora de Inicio.", help_text = "Ingrese la hora de inicio.", blank = False, null = False )
	horafin = models.TimeField(verbose_name = "Hora de finalización", help_text = "Ingrese la hora de finalización", blank = False, null = False )
	profesional = models.ForeignKey(Profesional, verbose_name = "Seleccione a un Profesional")
	servicio = models.ForeignKey(Servicio, verbose_name = "Seleccione un servicio")
	usuariox = models.ForeignKey(Usuario, verbose_name = "Usuario")
	estado = models.CharField(max_length = 1, verbose_name = "Estado", default = "D", blank = False, null = False)
	diaregistro = models.DateField(auto_now_add = True, verbose_name="Día de la semana", default = date.today())
	horaregistro = models.TimeField(auto_now_add = True, verbose_name = "Hora de registro.", default = datetime.now())
	#calcespecialidad = models.ManyToManyField(ProCalcSer, verbose_name = "Especialidad")
	def __unicode__(self):
		return  u'%s' % (str(self.profesional))

	def get_dia_text(self):
		if self.dia == date.today():
			return "Hoy"
		else:
			return str(self.dia.month) + " - " + str(self.dia.day) + " - " + str(self.dia.year)

	class Meta:
		app_label = 'clinica'

class PacienteCalc(models.Model):
	profesional = models.ForeignKey(ProfesionalCalc, verbose_name = "Seleccione a un Profesional")
	paciente = models.ForeignKey(Paciente, verbose_name = "Seleccione a un Paciente")
	precio = models.DecimalField(verbose_name = "Precio del servicio", max_digits = 7, decimal_places = 2, help_text = "Ingrese el precio del servicio.", blank = False, null = False)
	precio_bk = models.DecimalField(verbose_name = "Precio del servicio", max_digits = 7, decimal_places = 2, help_text = "Ingrese el precio del servicio.", default=0, blank = False, null = False)
	tipopago = models.CharField(max_length = 5, verbose_name = "Tipo Pago", default = "C", blank = False, null = False) # cash o tarjeta
	estado = models.CharField(max_length = 1, verbose_name = "Estado", default = "A", blank = False, null = False)
	usuariox = models.ForeignKey(Usuario, verbose_name = "Usuario")
	diaregistro = models.DateField(auto_now_add = True, verbose_name="Día de la semana", default = date.today())
	horaregistro = models.TimeField(auto_now_add = True, verbose_name = "Hora de registro.", default = datetime.now())
	ispago = models.CharField(max_length = 1, verbose_name="Pago realizado",  default = "F", blank = False, null = False)
	observacion = models.TextField(max_length = 500, verbose_name = "Observación", help_text = "Ingrese la observación", null = False, blank = False)
	diacambio = models.DateTimeField(auto_now = True, verbose_name = "Hora de registro.", default = datetime.now())
	tipocita = models.CharField(max_length = 5, verbose_name = "Tipo", default = "C", blank = False, null = False) # consulta o atencion, cita

	def __unicode__(self):
		return str(self.profesional.id) + "." + smart_unicode(self.profesional) + " - " + smart_unicode(self.paciente)

	def get_Tipocita(self):
		if self.tipocita == "C": return "Consulta"
		elif self.tipocita == "E": return smart_str("Evaluación")
		else: return ""

	class Meta:
		app_label = 'clinica'

class Paquete(models.Model):
	nombre = models.CharField(max_length = 120, verbose_name = "Nombre del paquete", help_text = "Ingrese el Nombre del paquete (max: 100 caracteres)", null = False, blank = False)
	disponible = models.BooleanField(default = True, verbose_name = "Disponible", help_text = "Indique la disponibilidad.", null = False, blank = False)
	precio = models.DecimalField(verbose_name = "Precio del paquete", max_digits = 7, decimal_places = 2, help_text = "Ingrese el precio del paquete.", blank = False, null = False, default = 0.00)
	def __unicode__(self):
		return u'%s' % (self.nombre)

	class Meta:
		app_label = 'clinica'

	def get_Features(self):
		doReturn = []
		for x in PaqueteServicio.objects.filter(paquete=self.id).values_list('servicio__nombre').order_by().annotate(Count('servicio')):
			doReturn.append({
				'servicio' : x[0],
				'cantidad' : x[1]
			})
		return doReturn

class PaqueteServicio(models.Model):
	paquete = models.ForeignKey(Paquete, verbose_name = "Seleccione a un Paquete")
	servicio = models.ForeignKey(Servicio, verbose_name = "Seleccione a un Paquete")
	orden = models.PositiveIntegerField(verbose_name = "Posición", help_text = "Ingrese un número para el orden.", null = False, blank = False, default = 0)
	def __unicode__(self):
		return u'%s' % (str(self.id) + " " + smart_str(self.paquete))

	class Meta:
		app_label = 'clinica'

class PacientePaquete(models.Model):
	paquete = models.ForeignKey(Paquete, verbose_name = "Seleccione Paquete")
	paciente = models.ForeignKey(Paciente, verbose_name = "Seleccione un Paciente")
	usuariox = models.ForeignKey(Usuario, verbose_name = "Usuario")
	diaregistro = models.DateField(auto_now_add = True, verbose_name="Día de la semana", default = date.today())
	horaregistro = models.TimeField(auto_now_add = True, verbose_name = "Hora de registro.", default = datetime.now())
	ispago = models.CharField(max_length = 1, verbose_name="Pago realizado",  default = "F", blank = False, null = False)
	estado = models.CharField(max_length = 1, verbose_name = "Estado", default = "A", blank = False, null = False)
	
	def __unicode__(self):
		return smart_unicode(self.paciente) + " - " + smart_unicode(self.paquete)

	class Meta:
		app_label = 'clinica'

	def get_Pagos(self):
		xPrecio = 0
		for xN in InFromPacPaq.objects.filter(paquete = self.id):
			xPrecio += xN.precio
		return self.paquete.precio - xPrecio

	def get_Pagos_json(self):
		doReturn = []
		for xN in InFromPacPaq.objects.filter(paquete = self.id):
			doReturn.append({
				'fecha' : xN.fechaemision.strftime("%Y-%m-%d"),
				'monto' : xN.precio,
			})
		return doReturn

	def get_Pagos_status_json(self):
		xPrecio = 0
		for xN in InFromPacPaq.objects.filter(paquete = self.id):
			xPrecio += xN.precio
		return {
			'precio' : self.paquete.precio,
			'pagado' : xPrecio,
			'debe' : self.paquete.precio - xPrecio
		}

	def get_Sesions(self):
		doReturn = []
		for sesion in PaqueteServicio.objects.filter(paquete = self.paquete.id).order_by('orden'):
			pqs = PacPaqSer.objects.filter(paquete = self.id, paqser = sesion.id).order_by('-diaregistro', '-horaregistro')[:1]
			if pqs:
				pqs = {
					'fecharegistro' : pqs[0].diaregistro,
					'fechasesion' : pqs[0].servicio.dia,
					'estado' : pqs[0].estado,
					'terapeuta_text' : pqs[0].get_Terapeuta_text(),
					'id' : pqs[0].id
				}
			doReturn.append({
				'orden' : sesion.orden,
				'sesionid' : sesion.id,
				'sesion' : sesion.servicio.nombre,
				'reservas' : pqs
			})
		return doReturn

	def get_Informes(self):
		doReturn = []
		for sesion in PaqueteServicio.objects.filter(paquete = self.paquete.id).order_by('orden'):
			pqs = PacPaqSer.objects.filter(paquete = self.id, paqser = sesion.id, estado = "A").order_by('-diaregistro', '-horaregistro')[:1]
			if pqs:
				_tmpInf = PacientePaqueteInforme.objects.filter(pacpaqser = pqs[0])
				if _tmpInf:
					_tmpInf = _tmpInf[len(_tmpInf)-1]
					doReturn.append({
						'informe' : _tmpInf
					})
		
		return doReturn

class PacPaqSer(models.Model):
	paquete = models.ForeignKey(PacientePaquete, verbose_name = "Seleccione Paquete")
	servicio = models.ForeignKey(ProfesionalCalc, verbose_name = "Seleccione un ProfesioanlCalc")
	paqser = models.ForeignKey(PaqueteServicio, verbose_name = "Seleccione un Paquete Servicio")
	#T: Pago, F: Falto
	ispago = models.CharField(max_length = 1, verbose_name="Pago realizado",  default = "F", blank = False, null = False)
	#A: asistio, P: postergo, F: Falto
	estado = models.CharField(max_length = 1, verbose_name = "Estado", default = "A", blank = False, null = False)
	precio = models.DecimalField(verbose_name = "Precio del servicio", max_digits = 7, decimal_places = 2, help_text = "Ingrese el precio del servicio.", blank = False, null = False)
	usuariox = models.ForeignKey(Usuario, verbose_name = "Usuario")
	diaregistro = models.DateField(auto_now_add = True, verbose_name="Día de la semana", default = datetime.now())
	horaregistro = models.TimeField(auto_now_add = True, verbose_name = "Hora de registro.", default = datetime.now())
	orden = models.PositiveIntegerField(verbose_name = "Posición", help_text = "Ingrese un número para el orden.", null = False, blank = False)
	
	def __unicode__(self):
		return  u'%s' % (self.servicio)

	class Meta:
		app_label = 'clinica'

	def get_Terapeuta_text(self):
		return self.servicio.profesional.get_Pseudo()

	def get_Estado_text(self):
		if self.estado == "A":
			return "Atendido"
		elif self.estado == "R":
			return "Reservado"
		elif self.estado == "P":
			return "Postergado"
		elif self.estado == "F":
			return "Faltó"
		elif self.estado == "Q":
			return "Cancelado"

class InFromPacPaq(models.Model):
	paquete = models.ForeignKey(PacientePaquete, verbose_name = "Seleccione Paquete")
	paciente = models.ForeignKey(Paciente, verbose_name = "Seleccione a un Paciente")
	estado = models.CharField(max_length = 1, verbose_name = "Estado", default = "I", blank = False, null = False)
	cerrado = models.CharField(max_length = 1, verbose_name = "Cerrado", default = "F", blank = False, null = False)
	precio = models.DecimalField(verbose_name = "Monto", max_digits = 7, decimal_places = 2, help_text = "Ingrese el Monto Pagado.", blank = False, null = False)
	numeroboleta = models.IntegerField(verbose_name = "Boleta", help_text = "Ingrese un número de boleta.", default=-1, null = False, blank = False)
	numerofactura = models.IntegerField(verbose_name = "RUC", help_text = "Ingrese un número de Ruc del cliente.", default=-1, null = False, blank = False)
	nombrePagador = models.CharField(max_length = 250, verbose_name = "Persona que paga", help_text = "Nombre de la persona que paga", null = False, blank = False)
	fechaemision = models.DateField(verbose_name = "Hora de Emisión", default = datetime.now(), null = False, blank = False)
	fechaeprox = models.DateField(verbose_name = "Fecha próximade pago", default = datetime.now(), null = False, blank = False)
	retencion =  models.IntegerField(verbose_name = "Retención", help_text = "Ingrese la retención.", default=-1, null = False, blank = False)
	usuariox = models.ForeignKey(Usuario, verbose_name = "Usuario")
	horaregistro = models.DateTimeField(auto_now_add = True, verbose_name = "Hora de registro.", default = datetime.now())
	#estadop = models.CharField(max_length = 1, verbose_name = "Tipo", default = "I", blank = False, null = False)
	def __unicode__(self):
		return u'%s' % ("PagoPaquete")

	class Meta:
		app_label = 'clinica'

class InFromPacCi(models.Model):
	cita = models.ForeignKey(PacienteCalc, verbose_name = "Seleccione Paciente Cita")
	paciente = models.ForeignKey(Paciente, verbose_name = "Seleccione a un Paciente")
	estado = models.CharField(max_length = 1, verbose_name = "Estado", default = "I", blank = False, null = False)
	cerrado = models.CharField(max_length = 1, verbose_name = "Cerrado", default = "F", blank = False, null = False)
	precio = models.DecimalField(verbose_name = "Monto", max_digits = 7, decimal_places = 2, help_text = "Ingrese el Monto Pagado.", blank = False, null = False)
	numeroboleta = models.IntegerField(verbose_name = "Boleta", help_text = "Ingrese un número de boleta.", default=-1, null = False, blank = False)
	numerofactura = models.IntegerField(verbose_name = "RUC", help_text = "Ingrese un número de Ruc del cliente.", default=-1, null = False, blank = False)
	nombrePagador = models.CharField(max_length = 250, verbose_name = "Persona que paga", help_text = "Nombre de la persona que paga", default="none", null = False, blank = False)
	fechaemision = models.DateField(verbose_name = "Hora de Emisión", default = datetime.now(), null = False, blank = False)
	retencion =  models.IntegerField(verbose_name = "Retención", help_text = "Ingrese la retención.", default=-1, null = False, blank = False)
	usuario = models.ForeignKey(Usuario, verbose_name = "Usuario")
	horaregistro = models.DateTimeField(auto_now_add = True, verbose_name = "Hora de registro.", default = datetime.now())
	#estadop = models.CharField(max_length = 1, verbose_name = "Tipo", default = "I", blank = False, null = False)
	def __unicode__(self):
		return u'%s' % ("PagoCita")

	class Meta:
		app_label = 'clinica'

class InOutPut(models.Model):
	concepto = models.TextField(max_length = 500, verbose_name = "Concepto del Pago", help_text = "Ingrese el Concepto", null = False, blank = False)
	estado = models.CharField(max_length = 1, verbose_name = "Tipo", default = "I", blank = False, null = False)
	cerrado = models.CharField(max_length = 1, verbose_name = "Cerrado", default = "F", blank = False, null = False)
	precio = models.DecimalField(verbose_name = "Monto", max_digits = 7, decimal_places = 2, help_text = "Ingrese el Monto pagado.", blank = False, null = False)
	numeroboleta = models.IntegerField(verbose_name = "Boleta", help_text = "Ingrese un número de boleta.", default=-1, null = False, blank = False)
	numerofactura = models.IntegerField(verbose_name = "RUC", help_text = "Ingrese un número de Ruc del cliente.", default=-1, null = False, blank = False)
	nombrePagador = models.CharField(max_length = 250, verbose_name = "Persona", help_text = "Nombre de la persona.", null = False, blank = False)
	#fechaemision = models.DateField(verbose_name = "Hora de Emisión", default = datetime.date.today(), null = False, blank = False)strftime("%Y-%m-%d")
	fechaemision = models.DateField(verbose_name = "Hora de Emisión (30/05/2012)", default = date.today().strftime("%d/%m/%Y"), null = False, blank = False)
	retencion =  models.IntegerField(verbose_name = "Retención", help_text = "Ingrese la retención.", default=-1, null = False, blank = False)
	usuario = models.ForeignKey(Usuario, verbose_name = "Usuario")
	horaregistro = models.DateTimeField(auto_now_add = True, verbose_name = "Hora de registro.", default = datetime.now())
	#estadop = models.CharField(max_length = 1, verbose_name = "Tipo", default = "I", blank = False, null = False)
	def __unicode__(self):
		return u'%s' % ("PagoIngresoDistinto")

	class Meta:
		app_label = 'clinica'