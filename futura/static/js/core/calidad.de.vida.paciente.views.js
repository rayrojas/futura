(function(){
	'use strict';
	CDV.Views.Paciente.search_exec = undefined;
	CDV.Views.Paciente.search = Backbone.View.extend({
		events: {
		},
		initialize: function(options) {
			this.generals = options;
			this.render();
		},
		render: function() {
			var $viewContext = this;
			$viewContext.$el.html(Handlebars.compile($("#v-p-cr-frm-srch-pct-tmpl-001").text())([]));
			$viewContext.$el.find("#v-g-i-frm-srch-pct-tmpl-inp-dni").autocomplete({
				'serviceUrl' : '/bk/paciente/search/dni.json',
				minChars: 4,
				delimiter: /(,|;)\s*/,
				nosuggestions: function() {},
				onSelect: function(value, data){
					$context.navigate('paciente/' + data, {trigger: true});
				}
			});
			$viewContext.$el.find("#v-g-i-frm-srch-pct-tmpl-inp-app").autocomplete({
				'serviceUrl' : '/bk/paciente/search/nombres.json',
				minChars: 4,
				delimiter: /(,|;)\s*/,
				nosuggestions: function() {},
				onSelect: function(value, data){
					$context.navigate('paciente/' + data, {trigger: true});
				}
			});
		}
	});
})();

(function(){
	'use strict';
	CDV.Views.Paciente.show_exec = undefined;
	CDV.Views.Paciente.show = Backbone.View.extend({
		events: {
		},
		initialize: function(options) {
			this.generals = options;
			this.render();
		},
		render: function() {
			var $viewContext = this;
			$.getter({
				'type' : 'api',
				'warning' : "error",
				'url' : 'r/paciente',
				'params' : '&dni=' + $viewContext['generals']['dni'],
				'success' : function(request) {
					$viewContext.$el.html(Handlebars.compile($("#v-p-cr-frm-nuevo-003").text())(request['objects'][0]));
					if (typeof($viewContext['generals']['do_render']) == "function") {
						$viewContext['generals']['do_render']();
					} else {
						$viewContext.$el.find('.node-street:first').focus();
					}
					/*if ( $viewContext['generals']['focus'] ) {
						
					}*/
				}
			});
		}
	});
})();

(function(){
	'use strict';
	CDV.Views.Paciente.nuevo = Backbone.View.extend({
		events: {
		},
		initialize: function() {
			this.render();
		},
		render: function() {
			var $viewContext = this;
			$viewContext.$el.html(Handlebars.compile($("#v-p-cr-frm-nuevo-002").text())([]));
		}
	});
})();

(function(){
	'use strict';
	CDV.Views.Paciente.Cita.search = Backbone.View.extend({
		events: {
			//'click #v-g-i-cts-tmpl-frm-srch-btn-trn': 'select_turn',
			//'click #v-g-i-cts-tmpl-frm-srch-btn-do': 'search',
			'click .v-c-s-l-s-001-tmpl-data-link' : 'event_select_servicio',
			'click .v-p-c-l-t-002-tmpl-data-link-001' : 'event_select_home',
			'click .v-p-c-l-t-002-tmpl-data-link-002' : 'event_select_terapeuta'
		},
		el : '#tmpl-frm-srch-cts',
		initialize: function(options) {
			this.generals = options;
			this.render();
		},
		render: function() {
			var $viewContext = this;
			$.getter({
				'type' : 'api',
				'url' : 'r/servicio',
				'complete' : function(request) {
					$viewContext.$el.html(Handlebars.compile($("#v-p-c-list-srv-001").text())(request));
					$viewContext.$el.find("ul li a.node-street:first").focus();
					$("#v-c-s-l-s-001-tmpl-inpt").autocomplete({
						'serviceUrl' : '/bk/servicio/search.json',
						minChars: 3,
						delimiter: /(,|;)\s*/,
						onSelect: function(value, data){
							$viewContext.fn_render_terapeutas(data);
						}
					});
				}
			});
		},
		event_select_servicio: function(event) {
			var $viewContext = this;
			var $this = $(event.currentTarget);
			$viewContext.fn_render_terapeutas($this.data('id'));
			return false;
		},
		fn_render_terapeutas: function(srv) {
			var $viewContext = this;
			$.getter({
				'type' : 'api',
				'url' : 'rdp/servicio',
				'params' : '&id=' + srv,
				'complete' : function(request) {
					$viewContext.$el.html(Handlebars.compile($("#v-p-c-list-trpt-002").text())(request['objects'][0]));
					$viewContext.$el.find(".onerow:first .col11 a.node-street:first").focus();
					/*$viewContext.$el.find('.v-p-c-l-t-002-tmpl-data-link-001').on("click", function(){
						$viewContext.render();
						return false;
					});*/
				}
			});			
		},
		/*select_turn: function() {
			var $this = $(this);
			if ( $this.data("max") == "12:00" ) {
				$this.data("max" , "23:00").data("min", "13:00").text("Tarde");
			} else if( $this.data("max") == "23:00" ) {
				$this.data("max" , "12:00").data("min", "7:00").text("Mañana");
			}
			return false;
		},*/
		/*search: function() {
			this.render_Servicios('rved/cupos',
							'&servicio=' + $("#v-g-i-cts-tmpl-frm-srch-inp-srv").data("value") +
							"&dia=" + $("#v-g-i-cts-tmpl-frm-srch-inp-dte").val() +
							"&horainicio__lte=" + $("#v-g-i-cts-tmpl-frm-srch-btn-trn").data("max") + 
							"&horainicio__gte=" + $("#v-g-i-cts-tmpl-frm-srch-btn-trn").data("min"));
			return false;
		},*/
		/*render_servicios: function(url, params) {
			$.getter({
				'type' : 'api',
				'url': url,
				'warning' : 'No se encontraron cupos disponibles.',
				'params' : params,
				success: function(request) {
					$("#table-cupos").html(Handlebars.compile($("#v-g-i-lst-cps-dps-tmpl").text())(request));
					$("#table-cupos .terapeuta-cupo").on("click", function(){
						$this = $(this);
						keys = $this.data();
						keys['token'] = getCookie('csrftoken');
						keys['paciente'] = $context['params']['dni'],
						$("#sidebar-frm-cita").html(Handlebars.compile($("#v-g-i-frm-rgt-cta").text())(keys))
						actionValidate($("#sidebar-frm-cita form"));
						$("#sidebar-frm-cita form").zenky({
							'url' : '/bk/paciente/cita/save.json/',
							'success' : function(request){

							}
						});
						$("#sidebar-frm-cita input[type=button]").on("click", function() {
							$.sidr('close', 'sidebar-frm-cita');
						});
						$.sidr('open', 'sidebar-frm-cita');
						$("#sidebar-frm-cita input[name=precio]").focus();
						return false;
					});
				},
				empty: function() {
					$("#table-cupos").html(Handlebars.compile($("#v-g-i-lst-cps-dps-tmpl").text())([]));
				}
			})
		},*/
		event_select_terapeuta: function(event) {
			var $viewContext = this;
			var $this = $(event.currentTarget);
			$viewContext.fn_select_terapeuta($this.data('id'));
			return false;
		},
		fn_select_terapeuta: function(id) {
			var $viewContext = this;
			var url = '/bk/profesional/disponible/calendar.json';
			if ( $viewContext.$el.find('.v-p-c-c-t-003-tmpl-calendar').length ) {
				$viewContext.generals['calendar'].fullCalendar('removeEventSource', $viewContext.generals['url_calendar']);
				$viewContext.generals['url_calendar'] = url + '?profesional=' + id;
				$viewContext.generals['calendar'].fullCalendar('addEventSource', $viewContext.generals['url_calendar']);
				$viewContext.generals['calendar'].fullCalendar('today');
			} else {
				$viewContext.generals['url_calendar'] = url + '?profesional=' + id;
				$viewContext.$el.find("#v-p-c-l-t-002-tmpl-area-calendar").html($("#v-p-c-calc-trpt-003").text());
				$viewContext.generals['calendar'] = $viewContext.$el.find('.v-p-c-c-t-003-tmpl-calendar').fullCalendar({
					events : $viewContext.generals['url_calendar'],
					allDaySlot: false,
					monthNames : ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort : ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
					dayNames : ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
					dayNamesShort : ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
					minTime : 8,
					maxTime : 22,
					lazyFetching: false,
					header: {
						left : 'title',
						right : '        prev,today,next month'
					},
					buttonText : {
						today :'Hoy',
						month :'Mes',
						week :'Semana',
						day :'Día'
					},
					eventClick: function(calEvent, jsEvent, view) {
						if ( view.name == "agendaDay" ) {
							if ( !$("#sidebar-frm-cita").length ) {
								$('body').sidr({
									'name' : 'sidebar-frm-cita',
									 side: 'right',
									source: function(name) {
										return '';
									}
								});
							}
							$.getter({
								'type' : 'api',
								'url' : 'rved/cupos',
								'params' : '&id=' + calEvent.id,
								'success' : function(request) {
									$("#sidebar-frm-cita .body").html(Handlebars.compile($("#v-p-c-frm-cta-004").text())(request['objects'][0]));
									$.sidr('open', 'sidebar-frm-cita');
								}
							})
						} else if ( view.name == "month" ) {
							$viewContext.generals['calendar'].fullCalendar( 'changeView', "agendaDay" )
							$viewContext.generals['calendar'].fullCalendar('gotoDate', calEvent.start)
						}

						//.fullCalendar('gotoDate', $.fullCalendar.formatDate(calEvent.start, 'yyyy'));
						/*alert('Event: ' + calEvent.title);
						alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
						alert('View: ' + view.name);*/

						// change the border color just for fun
						//$(this).css('border-color', 'red');


					},
					viewDisplay: function(view) {
						if (view.name == "agendaDay"){
							$viewContext.generals['calendar'].fullCalendar('option', 'height', 2400);
						} else {
							if ( $viewContext.generals['calendar'] != undefined ) $viewContext.generals['calendar'].fullCalendar('option', 'height', 500);
						}
					},
				});
			}
			$.getter({
				'type' : 'api',
				'url' : 'r/profesional',
				'params' : '&id=' + id,
				'success' : function(request) {
					$viewContext
					.$el
					.find('.v-p-c-c-t-003-tmpl-calendar .fc-header-center')
					.html('<a class="button small" style="background:#' + request['objects'][0]['color'] + '"><i class="icon-user"></i></a> ' + request['objects'][0]['first_name']);
				},
			})
			this.$el.find('.fc-header-center').text();			
			$("#tmpl-frm-srch-cts").scrollTop(this.$el.find('.v-p-c-c-t-003-tmpl-calendar').offset().top-20);
		},
		event_select_home: function() {
			this.render();
			return false;
		}
	});
})();

(function(){
	'use strict';
	CDV.Views.Paciente.Paquete.show_exec = undefined;
	CDV.Views.Paciente.Paquete.show = Backbone.View.extend({
		events : {
			'click .v-p-p-l-001-data-link-001' : 'event_show_paquete'
		},
		initialize: function(options) {
			this.generals = options;
			this.render();
		},
		render: function() {
			var $viewContext = this;
			$.getter({
				'type' : 'api',
				'url' : 'r/pacientepaquete',
				'params' : '&paciente_dni=' + $viewContext['generals']['dni'],
				success : function(request) {
					$viewContext.$el.html(Handlebars.compile($("#v-p-p-list-001").text())(request));
					if (typeof($viewContext['generals']['do_render']) == "function") {
						$viewContext['generals']['do_render']();
					} else {
						if ( $viewContext.$el.find(".v-p-p-l-001-data-link-001:first") ) $viewContext.$el.find(".v-p-p-l-001-data-link-001:first").focus();
						else $viewContext.$el.find(".v-p-p-l-001-data-link-002").focus();
					}
				}
			})
		},
		event_show_paquete: function(event){
			var $viewContext = this;
			var $this = $(event.currentTarget);
			$viewContext.fn_show_paquete($this.data('id'));
			console.log("asdasdxxxxxx");
			return false;
		},
		fn_show_paquete: function(id) {
			var $viewContext = this;
			CDV.Views.Paciente.Paquete.details_exec = new CDV.Views.Paciente.Paquete.details({el : $("#tmpl-frm-srch-cts"), id : id});
			CDV.Routers.index_exec.navigate('paciente/' + $viewContext['generals']['dni'] + '/paquete/' + id, {triger : false});
			/*$.getter({
				'type' : 'api',
				'url' : 'rd/pacientepaquete',
				'params' : '&id=' + id,
				'success' : function(request){
					$.each(request['objects'][0]['sesiones'], function(i, data){
						if ( data['reservas']['estado'] == "A" ) {
							data['estado_text'] = 'icon-thumbs-up'
						} else if ( data['reservas']['estado'] == "F" ) {
							data['estado_text'] = 'icon-thumbs-down red'
						} else if ( data['reservas']['estado'] == "R" ) {
							data['estado_text'] = 'icon-time'
						} else if ( data['reservas']['estado'] == "P" ) {
							data['estado_text'] = 'icon-minus-sign red'
						}
					});
				}
			})*/
		}
	});
})();

(function(){
	'use strict';
	console.log("estr");
	CDV.Views.Paciente.Paquete.details_exec = undefined;
	CDV.Views.Paciente.Paquete.details = Backbone.View.extend({
		events : {
			'click .v-p-p-p-d-002-tmpl-data-link-001' : 'event_show_sesion'
		},
		initialize: function(options) {
			this.generals = options;
			this.render();
		},
		render: function() {
			var $viewContext = this;
			$.getter({
				'type' : 'api',
				'url' : 'rd/pacientepaquete',
				'params' : '&id=' + $viewContext['generals']['id'],
				success : function(request) {
					$.each(request['objects'][0]['sesiones'], function(i, data){
						if ( data['reservas']['estado'] == "A" ) {
							data['estado_text'] = 'icon-thumbs-up'
						} else if ( data['reservas']['estado'] == "F" ) {
							data['estado_text'] = 'icon-thumbs-down red'
						} else if ( data['reservas']['estado'] == "R" ) {
							data['estado_text'] = 'icon-time'
						} else if ( data['reservas']['estado'] == "P" ) {
							data['estado_text'] = 'icon-minus-sign red'
						}
					});
					$viewContext.$el.html(Handlebars.compile($("#v-p-p-detail-002").text())(request['objects'][0]));
					/*$viewContext.$el.html(Handlebars.compile($("#v-p-p-list-001").text())(request));
					if ( $viewContext.$el.find(".v-p-p-l-001-data-link-001:first") ) $viewContext.$el.find(".v-p-p-l-001-data-link-001:first").focus();
					else $viewContext.$el.find(".v-p-p-l-001-data-link-002").focus();*/
				}
			})
		},
		event_show_sesion : function(event) {
			var $this = $(event.currentTarget);
			this.fn_show_sesion($this.data("id"));
			return false;
		},
		fn_show_sesion : function(id){
			console.log(id);
		}
	});
})();