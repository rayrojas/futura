var vppp001 = Backbone.View.extend({
	events: {
		'click #v-g-i-lst-own-pqts-html .link-black[data-link=paquete]' : 'render_PacientePaquete',
		'click #tmpl-v-g-i-area-free .show-paquetes' : 'show_Paquetes',
	},
	el : '#tmpl-area-multiple',
	initialize: function() {
		this.render();
	},
	render: function() {
		$viewContext = this;
		$.getter({
			'type' : 'api',
			'warning' : "error",
			'url' : 'r/paciente',
			'params' : '&dni=' + $context['params']['dni'],
			'success' : function(request) {
				$viewContext.render_Paciente(request['objects'][0]);
				$.getter({
					'type' : 'api',
					'url' : 'r/pacientepaquete',
					'params' : '&paciente_dni=' + $context['params']['dni'],
					'success' : function(request) {
						$viewContext.$el.children("#tmpl-v-g-i-area-free").html(Handlebars.compile($("#v-g-i-lst-own-pqts-tmpl").text())(request));
						$("#v-g-i-lst-own-pqts-html .link-black[data-link=paquete]:first").focus();
					},
					'empty': function(request) {
						$.sidr('open', 'sidebar-paquetes');
						return false;
					}
				})
			}
		});
		$('body').sidr({
			'name' : 'sidebar-paquetes',
			 side: 'left',
			source: function(name) {
				return ''
			}
		});
		$('body').sidr({
			'name' : 'sidebar-own-paquete',
			 side: 'right',
			source: function(name) {
				return ''
			}
		});
	},
	render_Paciente: function(data) {
		$viewContext = this;
		$viewContext.$el.children("#tmpl-v-g-i-dts-pct").html(Handlebars.compile($($("#tmpl-v-g-i-dts-pct").data("tmpl")).text())(data));
	},

	show_Paquetes: function() {
		$.sidr('open', 'sidebar-paquetes');
		return false;	
	},
	render_PacientePaquete: function(event) {
		$this = $(event.currentTarget);
		$.getter({
			'type' : 'api',
			'url' : 'rd/pacientepaquete',
			'params' : '&id=' + $this.data("id"),
			'success' : function(request) {
				$.each(request['objects'][0]['sesiones'], function(i, data){
					if ( data['reservas']['estado'] == "A" ) {
						data['estado_text'] = 'icon-thumbs-up'
					} else if ( data['reservas']['estado'] == "F" ) {
						data['estado_text'] = 'icon-thumbs-down red'
					} else if ( data['reservas']['estado'] == "R" ) {
						data['estado_text'] = 'icon-time'
					} else if ( data['reservas']['estado'] == "P" ) {
						data['estado_text'] = 'icon-minus-sign red'
					}
				})
				vpp002_exec = new vpp002({
					data : request['objects'][0]
				});
			},
			error : function() {

			}
		});
		return false;
	}
});

var vpp002 = Backbone.View.extend({
	events : {
		'click .data-link[data-link=sesion]' : 'render_Sesion',
		'click .data-link[data-link=pago-sesion]' : 'render_PagoSesion',
	},
	el : '#sidebar-own-paquete',
	initialize: function(options) {
		this.render(options['data']);
	},
	render: function(data) {
		$viewContext = this;
		$viewContext.$el.html(Handlebars.compile($("#v-g-i-pqt-detail-tmpl").text())(data));
		$("#sidebar-own-paquete").css("width", 650);
		$.sidr('open', 'sidebar-own-paquete');
		$("#sidebar-own-paquete .data-link[data-link=sesion]:first").focus();
	},
	render_PagoSesion: function(event){
		$viewContext = this;
		$this = $(event.currentTarget);
		function InternalRender() {
			$.getter({
				'type' : 'api',
				'url' : 'rdp/pacientepaquete',
				'params' : '&id=' + $this.data('id'),
				'success' : function(request) {
					request['objects'][0]['token'] = getCookie('csrftoken');
					$viewContext.$el.find('.container_12 .grid_6:last').html(
						Handlebars.compile($("#v-g-i-pqt-frm-pgo-tmpl").text())(request['objects'][0])
					);
					actionValidate($("#v-g-i-pqt-frm-pgo-tmpl-frm-rgt-pago"));
					$("#v-g-i-pqt-frm-pgo-tmpl-frm-rgt-pago").zenky({
						'url' : '/bk/paciente/paquete/pago/save.json/',
						'success' : function(request) {
							InternalRender();
						}
					});
					$("#v-g-i-pqt-frm-pgo-tmpl-frm-rgt-pago input[name=monto]").focus();
				},
				empty: function(request){
					request['objects'][0]['token'] = getCookie('csrftoken');
					$viewContext.$el.find('.container_12 .grid_6:last').html(
						Handlebars.compile($("#v-g-i-pqt-frm-pgo-tmpl").text())(request['objects'][0])
					);
					actionValidate($("#v-g-i-pqt-frm-pgo-tmpl-frm-rgt-pago"));
					$("#v-g-i-pqt-frm-pgo-tmpl-frm-rgt-pago").zenky({
						'url' : '/bk/paciente/paquete/pago/save.json/',
						'success' : function(request) {
							InternalRender();
						}
					});
					$("#v-g-i-pqt-frm-pgo-tmpl-frm-rgt-pago input[name=monto]").focus();
				}
			});

		}
		InternalRender();
		return false;
	},
	render_Sesion: function(event) {
		$viewContext = this;
		$this = $(event.currentTarget);
		if ( $this.data('id') ) {
			$.getter({
				'type' : 'api',
				'url' : 'r/sesion',
				'params' : '&id=' + $this.data('id'),
				'success' : function(request) {
					if ( request['objects'][0]['estado'] == 'R' ) {
							request['objects'][0]['is_reservado'] = true;
					} else if ( request['objects'][0]['estado'] == 'F' ) {
						request['objects'][0]['is_falto'] = true;
					} else if ( request['objects'][0]['estado'] == 'P' ) {
						request['objects'][0]['is_postergado'] = true;
					}
					$viewContext.$el.find('.container_12 .grid_6:last').html(
						Handlebars.compile($("#v-g-i-pqt-ssn-detail-tmpl").text())(request['objects'][0])
					);
				}
			});
		}
		return false;
	}
});