String.prototype.lpad = function(padString, length) {
	var str = this;
	while (str.length < length)
		str = padString + str;
	return str;
}
CDV.Views.Generals = {};
CDV.Views.Paciente = {};
CDV.Views.Paciente.Cita = {};
CDV.Views.Paciente.Paquete = {};

CDV.Routers.index = Backbone.Router.extend({
	routes: {
		"": "inicio",
		"paciente/nuevo": "paciente_nuevo",
		"paciente/:dni": "paciente",
		"paciente/:dni/cita": "paciente_cita",
		"paciente/:dni/paquete": "paciente_paquete",
		"paciente/:dni/paquete/:id": "paciente_paquete_show",
	},
	initialize: function () {
		$context = this;
		jwerty.key('→', function(){
			if ( $('.node-street:focus').length ) {
				elm = $('.node-street:focus');
				if ( elm.data('right') != undefined ) {
					eval(elm.data('right'));
				}
			}
		});
		jwerty.key('←', function(){
			if ( $('.node-street:focus').length ) {
				elm = $('.node-street:focus');
				if ( elm.data('left') != undefined ) {
					eval(elm.data('left'));
				}
			}
		});
		jwerty.key('↓', function(){
			if ( $('.node-street:focus').length ) {
				elm = $('.node-street:focus');
				if ( elm.data('down') != undefined ) {
					if ( elm.data('down') == 'parent-next-child' ) {
						elm.parent().next().children('.node-street').focus();
					} else if ( elm.data('down') == 'parent-parent-next-child' ) {
						elm.parent().parent().next().find('.node-street').focus();
					} else {
						eval(elm.data('down'));
					}
				}
			}
		});
		jwerty.key('↑', function(){
			if ( $('.node-street:focus').length ) {
				elm = $('.node-street:focus');
				if ( elm.data('up') != undefined ) {
					if ( elm.data('up') == 'parent-prev-child' ) {
						elm.parent().prev().children('.node-street').focus();
					} else if ( elm.data('down') == 'parent-parent-next-child' ) {
						elm.parent().parent().prev().find('.node-street').focus();
					} else {
						eval(elm.data('up'));
					}
				}
			}
		});
		jwerty.key('esc', function(){
			if ( $('.node-street:focus').length ) {
				elm = $('.node-street:focus');
				if ( elm.data('esc') != undefined ) {
					if ( elm.data('esc') == "go-home" ) {
						$context.navigate('/', {trigger: true});
					} else {
						eval(elm.data('esc'));
					}
				} else {
					$context.navigate('/', {trigger: true});
				}
			}
		});
		/*jwerty.key('→', function(){
			if ( $(".btn-key:focus").length ) {
				if ( $(".btn-key:focus").next('.btn-key').length ) {
					$(".btn-key:focus").next('.btn-key').focus();
				}
			} else if ( $("#v-g-i-frm-srch-pct-tmpl-inp-dni").is(":focus") ) {
				$("#v-g-i-frm-srch-pct-tmpl-inp-app").focus();
			} else if ( $("#v-g-i-cts-tmpl-frm-srch-inp-srv").is(":focus") ) {
				$("#v-g-i-cts-tmpl-frm-srch-btn-trn").focus();
			} else if ( $("#v-g-i-cts-tmpl-frm-srch-btn-trn").is(":focus") ) {
				$("#v-g-i-cts-tmpl-frm-srch-inp-dte").focus();
			} else if ( $("#sidebar-frm-cita input[type=submit]").is(":focus") ) {
				$("#sidebar-frm-cita input[type=button]").focus();
			} else if ( $("#sidebar-own-paquete .data-link[data-link=sesion]:focus").length ) {
				$("#sidebar-own-paquete .container_12 .grid_6 .data-link[data-link=accion-sesion]:first").focus();
			} else if ( $("#v-g-i-pqt-frm-pgo-tmpl-frm-rgt-pago input[type=submit]:focus").length ) {
				$("#v-g-i-pqt-frm-pgo-tmpl-frm-rgt-pago input[type=button]").focus();
			}
		});
		jwerty.key('←', function(){
			if ( $(".btn-key:focus").length ) {
				if ( $(".btn-key:focus").prev('.btn-key').length ) {
					$(".btn-key:focus").prev('.btn-key').focus();
				}
			} else if ( $("#v-g-i-cts-tmpl-frm-srch-btn-trn").is(":focus") ) {
				$("#v-g-i-cts-tmpl-frm-srch-inp-srv").focus();
			} else if ( $("#sidebar-frm-cita input[type=button]").is(":focus") ) {
				$("#sidebar-frm-cita input[type=submit]").focus();
			} else if ( $("#sidebar-own-paquete .container_12 .grid_6 .data-link[data-link=accion-sesion]:focus").length ) {
				$("#sidebar-own-paquete .data-link[data-link=sesion]:first").focus();
			} else if ( $("#v-g-i-pqt-frm-pgo-tmpl-frm-rgt-pago input[name=monto]:focus").length ) {
				$("#sidebar-own-paquete .data-link[data-link=sesion]:first").focus();
			} else if ( $("#v-g-i-pqt-frm-pgo-tmpl-frm-rgt-pago input[type=button]:focus").length ) {
				$("#v-g-i-pqt-frm-pgo-tmpl-frm-rgt-pago input[type=submit]").focus();
			}
		});
		jwerty.key('↩', function(){
			if ( $(".btn-key:focus").length ) {
				window.location.hash = $(".btn-key:focus").attr("href");
				return false;
			} else if ( $("#v-g-i-cts-tmpl-frm-srch-inp-dte").is(":focus") ) {
				$("#v-g-i-cts-tmpl-frm-srch-btn-do").focus();
			}
		});

		jwerty.key('↓', function(){
			if ( $("#v-g-i-frm-srch-pct-tmpl-inp-dni").is(":focus") || $("#v-g-i-frm-srch-pct-tmpl-inp-app").is(":focus") ) {
				if ( $("#v-g-i-dts-pct-tmpl-tbl-pct").length ) {
					dni = $("#v-g-i-dts-pct-tmpl-tbl-pct").data("dni").toString();
					$context.navigate('paciente/' + dni.lpad("0", 8), {trigger: true});
					$(".btn-key:first").focus();
					//window.location.hash = '#';
				}
			} else if ($("#v-g-i-cts-tmpl-frm-srch-inp-srv").is(":focus") || $("#v-g-i-cts-tmpl-frm-srch-btn-trn").is(":focus")) {
				$("#v-g-i-cts-tmpl-frm-srch-btn-do").focus();
			} else if ( $("#v-g-i-cts-tmpl-frm-srch-btn-do").is(":focus") ) {
				if ( $(".link-black.terapeuta-cupo").length ) {
					$(".link-black.terapeuta-cupo:first").focus();
				}
			} else if ( $(".link-black.terapeuta-cupo:focus").is(":focus") ) {
				if ( $(".link-black.terapeuta-cupo:focus").parent().parent().next('tr').length ) {
					$(".link-black.terapeuta-cupo:focus").parent().parent().next('tr').find('.terapeuta-cupo').focus();
				}
			} else if ( $("#sidebar-frm-cita input[name=precio]").is(":focus") ) {
				$("#sidebar-frm-cita textarea").focus();
			} else if ( $("#sidebar-frm-cita textarea").is(":focus") ) {
				$("#sidebar-frm-cita input[type=submit]").focus();
			} else if ( $("#v-g-i-lst-own-pqts-html .link-black[data-link=paquete]:focus").length ) {
				if ( $("#v-g-i-lst-own-pqts-html .link-black[data-link=paquete]:focus").parent().parent().next('tr').length ) {
					$("#v-g-i-lst-own-pqts-html .link-black[data-link=paquete]:focus").parent().parent().next('tr').find('.link-black[data-link=paquete]').focus();
				}
			} else if ( $("#sidebar-own-paquete .data-link[data-link=sesion]:focus").length ) {
				if ( $("#sidebar-own-paquete .data-link[data-link=sesion]:focus").parent().parent().next('tr').length ) {
					$("#sidebar-own-paquete .data-link[data-link=sesion]:focus").parent().parent().next('tr').find('.link-black[data-link=sesion]').focus();
				}
			} else if ( $("#sidebar-own-paquete .data-link[data-link=pago-sesion]:focus").length ) {
				$("#sidebar-own-paquete .data-link[data-link=sesion]:first").focus();
			} else if ( $("#v-g-i-pqt-frm-pgo-tmpl-frm-rgt-pago input[name=monto]:focus").length ) {
				$("#v-g-i-pqt-frm-pgo-tmpl-frm-rgt-pago input[type=submit]").focus();
			}*/

			/* else if ( $(".btn-key[data-link=paquete]").is(":focus") ) {
				console.log("asds");
				console.log($("#v-g-i-lst-own-pqts-html .link-black[data-link=paquete]"));
				$("#v-g-i-lst-own-pqts-html .link-black[data-link=paquete]:first").focus();
			}*/
		/*});
		jwerty.key('↑', function(){
			if ( $("#v-g-i-cts-tmpl-frm-srch-btn-do").is(":focus") ) {
				$("#v-g-i-cts-tmpl-frm-srch-inp-srv").focus();
			} else if ( $(".link-black.terapeuta-cupo:focus").is(":focus") ) {
				if ( $(".link-black.terapeuta-cupo:focus").parent().prev('tr').length ) {
					$(".link-black.terapeuta-cupo:focus").parent().parent().prev('tr').find('.terapeuta-cupo').focus();
				}
			} else if ( $("#sidebar-frm-cita input[type=button]").is(":focus") || $("#sidebar-frm-cita input[type=submit]").is(":focus") ) {
				$("#sidebar-frm-cita textarea").focus();
			} else if ( $("#sidebar-frm-cita textarea").is(":focus") ) {
				$("#sidebar-frm-cita input[name=precio]").focus();
			} else if ( $("#v-g-i-lst-own-pqts-html .link-black[data-link=paquete]:focus").length ) {
				if ( $("#v-g-i-lst-own-pqts-html .link-black[data-link=paquete]:focus").parent().parent().prev('tr').length ) {
					$("#v-g-i-lst-own-pqts-html .link-black[data-link=paquete]:focus").parent().parent().prev('tr').find('.link-black[data-link=paquete]').focus();
				}
			} else if ( $("#sidebar-own-paquete .data-link[data-link=sesion]:focus").length ) {
				if ( $("#sidebar-own-paquete .data-link[data-link=sesion]:focus").parent().parent().prev('tr').length ) {
					$("#sidebar-own-paquete .data-link[data-link=sesion]:focus").parent().parent().prev('tr').find('.link-black[data-link=sesion]').focus();
				} else if ( $("#sidebar-own-paquete .data-link[data-link=pago-sesion]").length ) {
					$("#sidebar-own-paquete .data-link[data-link=pago-sesion]").focus();
				}
			} else if ( $("#v-g-i-pqt-frm-pgo-tmpl-frm-rgt-pago input[type=submit]:focus").length || $("#v-g-i-pqt-frm-pgo-tmpl-frm-rgt-pago input[type=button]:focus").length ) {
				$("#v-g-i-pqt-frm-pgo-tmpl-frm-rgt-pago input[name=monto]").focus();
			}
		});
		*/
	},
	inicio: function (do_render) {
		require(['calidad.de.vida.generals.views',
					'calidad.de.vida.paciente.views'], function(){
			if ( CDV.Views.Generals.home_exec == undefined ) {
				CDV.Views.Generals.home_exec = new CDV.Views.Generals.home();
			} else {
				CDV.Views.Generals.home_exec.$el.find("#tmpl-frm-srch-cts").html("");
				CDV.Views.Generals.home_exec.$el.find("#tmpl-v-g-i-area-free").html("");
			}
			if ( CDV.Views.Paciente.search_exec == undefined ) {
				CDV.Views.Paciente.search_exec = new CDV.Views.Paciente.search({el: $("#tmpl-frm-srch-pct")});
			}
			if (typeof(do_render) == "function") {
				do_render();
			} else {
				$("#v-g-i-frm-srch-pct-tmpl-inp-dni").focus();
			}
		});
	},
	paciente: function(dni, do_render) {
		this.params = {
			'dni' : dni,
		}
		$context = this;
		$context.inicio(function(){
			require(['calidad.de.vida.paciente.views'], function(){
				if (typeof(do_render) == "function") {
					if ( CDV.Views.Paciente.show_exec == undefined ) {
						CDV.Views.Paciente.show_exec = new CDV.Views.Paciente.show({el: $("#tmpl-v-g-i-dts-pct"), 'dni' : $context['params']['dni'], 'focus' : true, do_render: do_render });
					} else {
						do_render();
					}
				} else {
					CDV.Views.Paciente.show_exec = new CDV.Views.Paciente.show({el: $("#tmpl-v-g-i-dts-pct"), 'dni' : $context['params']['dni'], 'focus' : true });
				}
			});
		});
	},
	paciente_nuevo: function() {
		$context = this;
		require(['calidad.de.vida.generals.views',
					'calidad.de.vida.paciente.views'], function(){
			new CDV.Views.Generals.crud();
			new CDV.Views.Paciente.nuevo({el: $('#v-p-cr-d-001-tmpl-area-one')});
		});
	},
	paciente_cita: function(dni) {
		this.params = {
			'dni' : dni,
		}
		$context = this;
		$context.paciente(dni, function(){
			if ( $(".v-p-c-tmpl").length ) {
				require(['calidad.de.vida.paciente.views'], function(){
					new CDV.Views.Paciente.Cita.search({'dni' : $context['params']['dni']});
				});
			} else {
				$.getter({
					'type' : 'views',
					'url' : 'paciente/cita.html',
					'warning': "No se pudo iniciar la aplicación",
					'success' : function(tmpl) {
						$("body").append(tmpl);
						require(['calidad.de.vida.paciente.views'], function(){
							new CDV.Views.Paciente.Cita.search({'dni' : $context['params']['dni']});
						});
					}
				});
			}
		});
	},
	paciente_paquete: function(dni, do_render) {
		this.params = {
			'dni' : dni,
		}
		$context = this;
		$context.paciente($context['params']['dni'], function(){
			if ( $(".v-p-p-tmpl").length ) {
				require(['calidad.de.vida.paciente.views'], function(){
					if (typeof(do_render) == "function") {
						/*if ( CDV.Views.Paciente.show_exec == undefined ) {
							CDV.Views.Paciente.Paquete.show_exec = new CDV.Views.Paciente.Paquete.show({'dni' : $context['params']['dni'], el : $("#tmpl-v-g-i-area-free"), do_render: do_render});
						} else {
							do_render();
						}*/
						CDV.Views.Paciente.Paquete.show_exec = new CDV.Views.Paciente.Paquete.show({'dni' : $context['params']['dni'], el : $("#tmpl-v-g-i-area-free"), do_render: do_render});
					} else {
						CDV.Views.Paciente.Paquete.show_exec = new CDV.Views.Paciente.Paquete.show({'dni' : $context['params']['dni'], el : $("#tmpl-v-g-i-area-free")});
					}
				});
			} else {
				$.getter({
					'type' : 'views',
					'url' : 'paciente/paquete.html',
					'warning': "No se pudo iniciar la aplicación",
					'success' : function(tmpl) {
						$("body").append(tmpl);
						require(['calidad.de.vida.paciente.views'], function(){
							CDV.Views.Paciente.Paquete.show_exec = new CDV.Views.Paciente.Paquete.show({'dni' : $context['params']['dni'], el : $("#tmpl-v-g-i-area-free")});
						});
						if (typeof(do_render) == "function") {
							do_render();
						}
					}
				});
			};
		});
	},
	paciente_paquete_show : function(dni, id) {
		this.params = {
			'dni' : dni,
			'id' : id,
		}
		$context = this;
		console.log("---------------");
		$context.paciente_paquete($context['params']['dni'], function() {
			console.log("asdsaddssd");
			CDV.Views.Paciente.Paquete.details_exec = new CDV.Views.Paciente.Paquete.details({el : $("#tmpl-frm-srch-cts"), id : id });
		});
	}
		/*this.params = {
			'dni' : dni,
			'paquete' : undefined,
			'sesion' : undefined,
		}
		$context = this;
		require(['calidad.de.vida.paquete.views'], function(){
			$context.inicio(function(){
				vpp001_exec = new vpp001();
			});
		});*/
});
CDV.Routers.index_exec = new CDV.Routers.index();
$(document).on("ready", function(){
	parallel([
		$.get("/static/views/generals/index.html"),
		$.get("/static/views/paciente/crud.html"),
	]).
	next(function (tmpl) {
		$("body").append(tmpl[0]);
		$("body").append(tmpl[1]);
		$('body').sidr({
			'name' : 'sidebar-help',
			 side: 'left',
			source: function(name) {
				return $("#v-g-i-sdb-help").text();
			}
		});
		$('body').sidr({
			'name' : 'sidebar-disponibles',
			 side: 'left',
			source: function(name) {
				return '';
			}
		});			
		Backbone.history.start();
		jwerty.key('f2', function(){
			if ( $("#sidebar-help").is(":visible") ) {
				$.sidr('close', 'sidebar-help');
			} else {
				$.sidr('open', 'sidebar-help');
			}
		});
		jwerty.key('f8', function(){
			if ( $("#sidebar-disponibles").is(":visible") ) {
				$.sidr('close', 'sidebar-disponibles');
			} else {
				$.get("/bk/profesional/disponible.json/")
				.success(function(request){
					$("#sidebar-disponibles .body").html(Handlebars.compile($("#v-g-i-lst-trp-dps-tmpl").text())(request));
					$.sidr('open', 'sidebar-disponibles');
				})
				.error(function(){
					
				});
			}
		});
		$("#btn-sidebar-help").on("click", function(){
			$.get("/bk/profesional/disponible.json/")
			.success(function(request){
				$("#sidebar-disponibles .body").html(Handlebars.compile($("#v-g-i-lst-trp-dps-tmpl").text())(request));
				$.sidr('open', 'sidebar-help');
			})
			.error(function(){
				
			});
			return false;
		});
		$("#btn-sidebar-disponibilidad").on("click", function(){
			$.sidr('open', 'sidebar-disponibles');
			return false;
		});
	});
});