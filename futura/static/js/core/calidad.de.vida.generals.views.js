(function(){
	'use strict';
	CDV.Views.Generals.home_exec = undefined;
	CDV.Views.Generals.home = Backbone.View.extend({
		events: {
		},
		el : '#main',
		initialize: function() {
			this.render();
		},
		render: function() {
			var $viewContext = this;
			$viewContext.$el.html(Handlebars.compile($("#v-g-i-dash-001").text())([]));
			$viewContext.$el.find("#tmpl-frm-srch-cts").css("height", $(document).height()-70).html("");
			$viewContext.$el.find("#tmpl-v-g-i-area-free").html("");
		}
	});
})();

(function(){
	'use strict';
	CDV.Views.Generals.crud_exec = undefined;
	CDV.Views.Generals.crud = Backbone.View.extend({
		events: {
		},
		el : '#main',
		initialize: function() {
			this.render();
		},
		render: function() {
			var $viewContext = this;
			$viewContext.$el.html(Handlebars.compile($("#v-g-i-dash-002").text())([]));
		}
	});
})();
