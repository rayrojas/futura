CDV = {};
CDV.Routers = {};
CDV.Views = {};

function getCookie(name) {
	var cookieValue = null;
	if (document.cookie && document.cookie != '') {
		var cookies = document.cookie.split(';');
		for (var i = 0; i < cookies.length; i++) {
			var cookie = jQuery.trim(cookies[i]);
			if (cookie.substring(0, name.length + 1) == (name + '=')) {
				cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
				break;
			}
		}
	}
	return cookieValue;
}

function callNotication( msg ){
	$.notification({
		title:"Calidad de Vida",
		content:msg,
		border:false
	});
}

function actionValidate(elm) {
	$(elm).validate({
		meta: 'validate',
	});
}

$(document).on("ready", function(){
	Deferred.define();
});