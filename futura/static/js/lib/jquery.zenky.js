// jQuery Plugin Boilerplate
// A boilerplate for jumpstarting jQuery plugins development
// version 1.1, May 14th, 2011
// by Stefan Gabos

(function($) {

	$.zenky = function(element, options) {

		var defaults = {
			foo: 'bar',
			url: null,
			before: null,
			success: null,
			postsuccess : null,
			error: null,
			values : null,
			prebefore : null,
			btnsubmit: '.btn-submit',
			onFoo: function() {}
		}

		var plugin = this;

		plugin.settings = {}

		var $element = $(element),
			 element = element;

		plugin.init = function() {
			plugin.settings = $.extend({}, defaults, options);
			$element.data("validator").settings.submitHandler = function(){
				if ($.isFunction(plugin.settings.prebefore)) {if (plugin.settings.prebefore() === false) return false;}
				$element.find(".load-proccess").removeClass("to-hide");
				$element.find(plugin.settings.btnsubmit).val("Enviando").attr("disabled",true);

				var o = {};
				if ( plugin.settings.values == null ) {
					var a = $element.find(".field");
					$.each(a, function(i, x) {
						item = $(x);
						if ( item.is("select") ) {
							o[item.attr("name")] = item.val();
						} else if ($(x).is(":radio")) {
							if (item.is(':checked')) o[item.attr("name")] = item.val();
						} else {
							o[$(x).attr("name")] = $(x).val();
						}
						
					});
				} else {
					o = plugin.settings.values;
				}
				
				if ($.isFunction(plugin.settings.before)) {if (plugin.settings.before() === false) return false;}
				$.post(plugin.settings.url, o)
				.success(function(request){
					$element.find(".load-proccess").addClass("to-hide");
					if ($.isFunction(plugin.settings.success)) {plugin.settings.success(request);}
					if ( request.kill != undefined ) { $element.find(plugin.settings.btnsubmit).addClass("to-hide"); return true;}
					if ( request.msg != undefined ) {
						$('#jgrowl').jGrowl(request.msg);
						$element.find(plugin.settings.btnsubmit).addClass("to-hide");
					} else {
						$element.find(plugin.settings.btnsubmit).addClass("to-hide");
					}
					if ($.isFunction(plugin.settings.postsuccess)) {plugin.settings.postsuccess(request);}
				})
				.error(function(){
					$('#jgrowl').jGrowl("No se pudo completar la operación");
					$element.find(".load-proccess").addClass("to-hide");
					$element.find(plugin.settings.btnsubmit).addClass("to-hide");
					if ($.isFunction(plugin.settings.error)) {plugin.settings.error();}					
				});
				return false;
			};
			return false;
			// code goes here
		}

		plugin.foo_public_method = function() {
			// code goes here
		}

		var foo_private_method = function() {
			// code goes here
		}

		plugin.init();

	}

	$.fn.zenky = function(options) {

		return this.each(function() {
			if (undefined == $(this).data('zenky')) {
				var plugin = new $.zenky(this, options);
				$(this).data('zenky', plugin);
			}
		});

	}

})(jQuery);