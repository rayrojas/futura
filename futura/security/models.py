#encoding:utf-8
from django.db import models
from django.contrib.auth.models import User, UserManager, Group
from django.utils.encoding import smart_str, smart_unicode

'''
class Institucion(models.Model):
	nombre = models.CharField(max_length=250, verbose_name = "Nombre de la institución.", blank = False, null = False)
	def __unicode__(self):
		return smart_str(self.nombre)
'''

class Usuario(User):
	#direccion = models.CharField(max_length = 250, verbose_name = "Dirección", help_text = "Ingrese la dirección.", null = False, blank = True)
	telfijo = models.CharField(max_length = 15, verbose_name = "Teléfono fijo", help_text = "Ingrese el teléfono fijo.", null = False, blank = True)
	telmobil = models.CharField(max_length = 15, verbose_name = "Teléfono móbil", help_text = "Ingrese el teléfono móvil.", null = False, blank = True)	
	#objects = UserManager()
	def __unicode__(self):
		return  u'%s' % (self.username + " - " + self.last_name + " " + self.first_name)

	class Meta:
		app_label = 'clinica'
		db_table = 'clinica_usuariox'

	def get_Grupos(self):
		grupos = ""
		for x in self.groups.all():
			grupos += ", "+str(x.name)
		return u'%s' % (grupos[2:len(grupos)])

	def get_Pseudo(self):
		if self.first_name and self.last_name:
			return (self.first_name[0] + self.last_name.split(" ")[0])
		else:
			return self.first_name + " " + self.last_name

	def get_nombres(self):
		return smart_str(self.last_name + " " + self.first_name)

	def get_active_check(self):
		if self.is_active:
			return "checked"
		else:
			return ""	

class MenuFirstLevel(models.Model):
	grupo = models.ForeignKey(Group, verbose_name = "Grupo")
	titulo = models.CharField(max_length = 50, verbose_name = "Titulo de la opción", help_text = "Ingrese el titulo de la opción.", null = False, blank = False)
	disponible = models.BooleanField(default = True, verbose_name = "Disponible", help_text = "Indique la disponibilidad.", null = False, blank = False)
	orden = models.PositiveIntegerField(verbose_name = "Posición", help_text = "Ingrese un número para el orden.", null = False, blank = False)
	link = models.CharField(max_length = 250, verbose_name = "Enlace de la opción", help_text = "Ingrese el enlace de la opción (URL)", null = False, blank = False)
	attr_class = models.CharField(max_length = 50, verbose_name = "Atrubuto Class", help_text = "Ingrese el atributo Class.", null = False, blank = True)
	icono = models.CharField(max_length = 50, verbose_name = "Icono", help_text = "Ingrese el icono de la opcion.", null = False, blank = True)
	def __unicode__(self):
		return u'%s' % (self.grupo.name + " - " + str(self.orden) + "-" + self.titulo)
		
class MenuSecondLevel(models.Model):
	permisos = models.ForeignKey(Group, verbose_name = "Permisos")
	titulo = models.CharField(max_length = 50, verbose_name = "Titulo de la opción", help_text = "Ingrese el titulo de la opción.", null = False, blank = False)
	disponible = models.BooleanField(default = True, verbose_name = "Disponible", help_text = "Indique la disponibilidad.", null = False, blank = False)
	orden = models.PositiveIntegerField(verbose_name = "Posición", help_text = "Ingrese un número para el orden.", null = False, blank = False)
	link = models.CharField(max_length = 250, verbose_name = "Enlace de la opción", help_text = "Ingrese el enlace de la opción (URL)", null = False, blank = False)
	def __unicode__(self):
		return u'%s' % (self.permisos.name + " - " + str(self.id) + " - " + self.titulo)
		
class MenuThirdLevel(models.Model):
	permisos = models.ForeignKey(Group, verbose_name = "Permisos")
	titulo = models.CharField(max_length = 50, verbose_name = "Titulo de la opción", help_text = "Ingrese el titulo de la opción.", null = False, blank = False)
	disponible = models.BooleanField(default = True, verbose_name = "Disponible", help_text = "Indique la disponibilidad.", null = False, blank = False)
	orden = models.PositiveIntegerField(verbose_name = "Posición", help_text = "Ingrese un número para el orden.", null = False, blank = False)
	link = models.CharField(max_length = 250, verbose_name = "Enlace de la opción", help_text = "Ingrese el enlace de la opción (URL)", null = False, blank = False)
	attr_class = models.CharField(max_length = 50, verbose_name = "Atrubuto Class", help_text = "Ingrese el atributo Class.", null = False, blank = True)
	icono = models.CharField(max_length = 50, verbose_name = "Icono", help_text = "Ingrese el icono de la opcion.", null = False, blank = True)
	def __unicode__(self):
		return u'%s' % (self.permisos.name + " - " + str(self.id) + " - " + self.titulo)

class Acciones(models.Model):
	grupo = models.PositiveIntegerField(verbose_name = "Grupo", help_text = "Ingrese un número para el orden.", null = False, blank = False)
	orden = models.PositiveIntegerField(verbose_name = "Posición", help_text = "Ingrese un número para el orden.", null = False, blank = False)
	cliente = models.CharField(max_length = 250, verbose_name = "Cliente", help_text = "Ingrese el Cliente.", null = False, blank = False)
	title = models.CharField(max_length = 250, verbose_name = "Titulo", help_text = "Ingrese el Titulo.", null = False, blank = False)
	icono = models.CharField(max_length = 50, verbose_name = "Icono", help_text = "Ingrese el icono de la opcion.", null = False, blank = True)
	text = models.CharField(max_length = 250, verbose_name = "text", help_text = "Ingrese el texto de la opcion.", null = False, blank = True)
	url = models.CharField(max_length = 250, verbose_name = "url", help_text = "Url.", null = False, blank = False)
	clase = models.CharField(max_length = 250, verbose_name = "class", help_text = "class", null = False, blank = True)
	ide = models.CharField(max_length = 250, verbose_name = "Id", help_text = "Id", null = False, blank = True)
	def __unicode__(self):
		return u'%s' % (str(self.grupo) + " - " + str(self.orden) + " - " + self.cliente + " - " + self.title + " - " + self.icono + " - " + self.text)