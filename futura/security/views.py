#encoding:utf-8
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils import simplejson
from django.contrib.auth.models import User, Group
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth import login as auth_login
from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse
from django.utils import simplejson
from security.models import Usuario, MenuFirstLevel, MenuSecondLevel, MenuThirdLevel
from security.forms import UsuarioForm, UsuarioFormEdit, UsuarioFormPass
import datetime
import time
import json

def getAplicacion():
	return {
		'root' : '/',
		'app' : {'nombre' : 'Sistema de Información Rosa de Lima'}
	}

def hasPermission(u, patron):
	for uu in u:
		if uu.id == patron:
			return True
	return False

def isLogin(u):
	if u is not None and u.is_active:
		return True
	else:
		return False

def getMenuFirst(g):
	items = []
	for grp in g:
		items.append(MenuFirstLevel.objects.filter(grupo = grp, disponible = True).order_by('orden'))
	return items

def getMenuSecond(g):
	items = []
	for grp in g:
		items.append(MenuSecondLevel.objects.filter(permisos = grp, disponible = True).order_by('orden'))
	return items

def getThirdSecond(g):
	items = []
	for grp in g:
		items.append(MenuThirdLevel.objects.filter(permisos = grp))
	return items

def index(request, codigo = ""):
	usuario = request.user
	if not (isLogin(usuario)) and (Usuario.objects.all()):
		return HttpResponseRedirect("/login/")
	if (hasPermission(usuario.groups.all(), 1)) or not (Usuario.objects.all()):
		Generals = {
			'aplicacion' : getAplicacion(),
			'items' : getMenuFirst(usuario.groups.all()),
			'menu' :  getMenuSecond(usuario.groups.all()),
			'titulo' : 'Formulario de Usuarios',
			'titulo_segundo' : "Lista de Usuarios",
			'action' : '/usuario/',
			'objetos' : Usuario.objects.exclude(groups=None).exclude(groups=7).order_by('groups__id'),
			'suject' : [],
			'mensaje' : "",
			'Url' : request.get_full_path(),
		}
		if request.method == 'POST':
			if codigo:
				try:
					Generals['suject'] = Usuario.objects.get(id = codigo)
					form = UsuarioFormEdit(request.POST, instance = Generals['suject'])
					Generals['mensaje'] = "Se modificó satisfactoriamente."
				except:
					logear(str(datetime.datetime.now()) + " - usuariox_registro" + " - UsuarioFormEdit(request.POST, instance = Usuario.objects.get(id = codigo))")
					return HttpResponseRedirect('/404/')
			else:
				Generals['mensaje'] = "Se registró satisfactoriamente."
				form = UsuarioForm(request.POST)
			if form.is_valid():
				if codigo:
					form.save()
				else:
					user = form.save()
					userX = form.cleaned_data['username']
					user.set_password(form.cleaned_data['password'])
					user.save()
				messages.success(request, Generals['mensaje'])
				return HttpResponseRedirect('/usuario/')
			else:
				if codigo:
					messages.error(request, "No se modificaron los datos.")
					codigo += "/"
				else:
					messages.error(request, "No se registraron los datos.")
				context = {
					'aplicacion' : Generals['aplicacion'],
					'items' : Generals['items'],
					'menu' :  Generals['menu'],
					'titulo' : Generals['titulo'],
					'titulo_segundo' : Generals['titulo_segundo'],	
					'action' : Generals['action'],
					'objetos' : Generals['objetos'],
					'form' : form,
					'codigo':codigo,
					'Url' : Generals['Url'],
				}
		else:
			if codigo:
				try:
					Generals['suject'] = Usuario.objects.get(id = codigo)
					form = UsuarioFormEdit(instance = Generals['suject'])
					codigo = str(codigo) + "/"
					messages.info(request, ("Editando datos de " + str(Generals['suject'].username) ))
				except:
					logear(str(datetime.datetime.now()) + " - usuariox_registro" + " - UsuarioFormEdit(instance = Usuario.objects.get(id = codigo))")
					return HttpResponseRedirect('/404/')
			else:
				form = UsuarioForm()
			context = {
					'aplicacion' : Generals['aplicacion'],
					'items' : Generals['items'],
					'menu' :  Generals['menu'],
					'titulo' : Generals['titulo'],
					'titulo_segundo' : Generals['titulo_segundo'],	
					'action' : Generals['action'],
					'objetos' : Generals['objetos'],
					'Url' : Generals['Url'],
					'codigo' : codigo,
					'form' : form,
			}
		return render_to_response('security/lista.html', context, context_instance = RequestContext(request))
	return HttpResponseRedirect('/')

def usuariox_lista(request):
	usuario = request.user
	if not (isLogin(usuario)):
		return HttpResponseRedirect("/login/")
	if (hasPermission(usuario.groups.all(), 1)):
		context = {
					'aplicacion' : getAplicacion(),
					'items' : getMenuFirst(usuario.groups.all()),
					'menu' :  getMenuSecond(usuario.groups.all()),
					'usuarioxs' : getUsuario(),
				}
		return render_to_response('usuariox-lista.html', context)
	return HttpResponseRedirect('/')

def usuariox_pass(request, codigo):
	usuario = request.user
	if not (isLogin(usuario)):
		return HttpResponseRedirect("/login/")
	if (hasPermission(usuario.groups.all(), 1)):
		Generals = {
			'aplicacion' : getAplicacion(),
			'items' : getMenuFirst(usuario.groups.all()),
			'menu' :  getMenuSecond(usuario.groups.all()),
			'titulo' : 'Formulario de Usuarios',
			'titulo_segundo' : "Lista de Usuarios",
			'action' : '/usuario/',
			'objetos' : Usuario.objects.all().order_by('last_name'),
			'suject' : [],
		}
		if request.method == 'POST':
			if codigo:
				try:
					Generals['suject'] = Usuario.objects.get(id = codigo)
					Generals['suject'].set_password(request.POST['password'])
					Generals['suject'].save()
					messages.success(request, ("Se modificó satisfactoriamente la contraseña de " + str(Generals['suject'].username) ))
				except:
					logear(str(datetime.datetime.now()) + " - usuariox_registro" + " - Usuario.objects.get(id = codigo)")
					return HttpResponseRedirect('/404/')
			return HttpResponseRedirect('/usuario/')
		else:
			Generals['suject'] = Usuario.objects.get(id = codigo)
			Generals['action'] += "password/"
			codigo += "/"
			form = UsuarioFormPass()
			messages.info(request, ("Editando la contraseña de " + str(Generals['suject'].username) ))
			context = {
					'aplicacion' : Generals['aplicacion'],
					'items' : Generals['items'],
					'menu' :  Generals['menu'],
					'titulo' : Generals['titulo'],
					'titulo_segundo' : Generals['titulo_segundo'],	
					'action' : Generals['action'],
					'objetos' : Generals['objetos'],
					'codigo' : codigo,
					'form' : form,
			}
		return render_to_response('security/lista.html', context, context_instance = RequestContext(request))
	return HttpResponseRedirect('/')
## Administrador : 1,
## Academico: 2,
## Academico Secretaria: 3
## Academico Secretaria Matriculas: 4
def is_administrador(request):
	usuario = request.user
	if not (isLogin(usuario)):
		return HttpResponse(status = 400)
	if (hasPermission(usuario.groups.all(), 1) or hasPermission(usuario.groups.all(), 2) or hasPermission(usuario.groups.all(), 3) or hasPermission(usuario.groups.all(), 4)):
		if request.method == 'POST':
			try:
				ustring = request.POST['passwd']
				if ustring:
					#administradores = Group.objects.get(id=1)
					for x in Usuario.objects.filter(is_active = True, groups = 1):
						#if administradores in x.groups.all():
						user = authenticate(username = x.username, password = ustring)
						if user is not None:
							return HttpResponse(status = 200)
					return HttpResponse(status = 400)
			except:
				return HttpResponse(status = 400)
	return HttpResponse(status = 400)