# -*- coding: utf-8 -*-
from django import forms
from django.db import models
from django.forms import ModelMultipleChoiceField
from django.forms.fields import MultipleChoiceField
from django.forms.widgets import SelectMultiple, PasswordInput, SplitDateTimeWidget, HiddenInput, Textarea, MultipleHiddenInput
from django.forms.extras.widgets import SelectDateWidget
from django.contrib.auth.models import Group
from django.utils import encoding
from django.utils.encoding import smart_str, smart_unicode
from security.models import Usuario

class UsuarioForm(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		super(UsuarioForm, self).__init__(*args, **kwargs) 
		self.fields['username'] = forms.CharField(label = "DNI", max_length = 8)
		self.fields['password'] = forms.CharField(label = "Contraseña", widget = forms.PasswordInput)
		self.fields['first_name'] = forms.CharField(label = "Nombres", max_length = 70) 
		self.fields['last_name'] = forms.CharField(label = "Apellidos", max_length = 70)
		self.fields['email'] = forms.EmailField(label = "E-mail", max_length = 250)
		self.fields.insert(5,'password_reply',forms.CharField(label = "Repita la Contraseña", widget = forms.PasswordInput))
		#Class validation
		self.fields['username'].widget.attrs['class'] = '{validate:{required:true, minlength:8, number: true}}';
		self.fields['password'].widget.attrs['class'] = '{validate:{required:true}}';
		self.fields['password_reply'].widget.attrs['class'] = '{validate:{equalTo:"#id_password"}}';
		self.fields['first_name'].widget.attrs['class'] = '{validate:{required:true}}';
		self.fields['last_name'].widget.attrs['class'] = '{validate:{required:true}}';
		self.fields['email'].widget.attrs['class'] = '{validate:{required:true, email:true}}';
		self.fields['telfijo'].widget.attrs['class'] = '{validate:{required:true, minlength:6, number: true}}';
		self.fields['telmobil'].widget.attrs['class'] = '{validate:{required:true, minlength:6, number: true}}';		
		self.fields['groups'] = forms.ModelMultipleChoiceField(label = "Grupos", help_text="Puede seleccionar más de una opción.", queryset = Group.objects.all())
		#The previous line is for customize or filter the fields to show.
	class Meta:
		model = Usuario
		exclude = ['is_staff', 'last_login', 'date_joined', 'is_superuser', 'user_permissions']

class UsuarioFormEdit(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		super(UsuarioFormEdit, self).__init__(*args, **kwargs) 
		self.fields['first_name'] = forms.CharField(label = "Nombres", max_length = 70) 
		self.fields['last_name'] = forms.CharField(label = "Apellidos", max_length = 70)
		self.fields['email'] = forms.EmailField(label = "E-mail", max_length = 250)
		#Class validation
		self.fields['first_name'].widget.attrs['class'] = '{validate:{required:true}}';
		self.fields['last_name'].widget.attrs['class'] = '{validate:{required:true}}';
		self.fields['email'].widget.attrs['class'] = '{validate:{required:true, email:true}}';
		self.fields['telfijo'].widget.attrs['class'] = '{validate:{required:true, minlength:6, number: true}}';
		self.fields['telmobil'].widget.attrs['class'] = '{validate:{required:true, minlength:6, number: true}}';
		self.fields['groups'] = forms.ModelMultipleChoiceField(label = "Grupos", help_text="Puede seleccionar más de una opción.", queryset = Group.objects.all())
		#self.fields['groups'] = forms.MultipleChoiceField(label = "Grupos", choices = getGroups(), widget = SelectMultiple)
		#The previous line is for customize or filter the fields to show.
	class Meta:
		model = Usuario
		exclude = ['is_staff', 'last_login', 'date_joined', 'is_superuser', 'user_permissions', 'password', 'username']


class UsuarioFormPass(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		super(UsuarioFormPass, self).__init__(*args, **kwargs) 
		self.fields['password'] = forms.CharField(label = "Contraseña", widget = forms.PasswordInput)
		self.fields.insert(5,'password_reply',forms.CharField(label = "Repita la Contraseña", widget = forms.PasswordInput))
		self.fields['password'].widget.attrs['class'] = '{validate:{required:true}}';
		self.fields['password_reply'].widget.attrs['class'] = '{validate:{equalTo:"#id_password"}}';
	class Meta:
		model = Usuario
		exclude = ['is_staff', 'is_active', 'last_login', 'date_joined', 'is_superuser', 'user_permissions', 'username', 'first_name',
					'last_name', 'email', 'telfijo' ,'telmobil', 'groups', 'direccion']

class LoginForm(forms.Form):
	username = forms.CharField(label = "DNI", max_length = 30)
	password = forms.CharField(label = "Contaseña", widget = forms.PasswordInput)
